# TIE-51257 Parallel Computing project
- Mika Mäki
- Samaneh Ammari

## Compilation

### Linux
Configure which source file you want to compile (parallel_original.c, parallel_openmp.c etc.)

`export SOURCE_FILE=parallel_original.c`

Run the script in the root folder of the repository

`./build_linux/build.sh`

For a faster development cycle you can build only one version with

`./build_linux/build_single.sh`

### Windows
It's recommended to run the build script using cmd (=the old Windows shell).
Configure which source file to compile with the command

`set SOURCE_FILE=parallel_original.c`

However, if using PowerShell, use the following command instead

`Set-Item -Path Env:SOURCE_FILE -Value "parallel_original.c"`

Run the script in the root folder of the repository

`.\build_windows_msvc\build.bat`

The OpenCL-enabled executables have to be run from the folder where the .cl files are located. This can be done using a command such as

`.\build\parallel_original.exe`

### Docker
Compilation for Linux

`docker build -f ./build_linux/Dockerfile .`

Compilation for Windows in a Linux container

`docker build -f ./build_windows/Dockerfile .`

In both scripts you should change the line `ARG SOURCE_FILE=parallel_original.c` to the version of the code you are compiling.
Output can be extracted by using docker cp.

### GitLab CI/CD
There are GitLab CI/CD scripts for compiling the code in the cloud.
You can access the CI/CD controls from the GitLab sidebar.
