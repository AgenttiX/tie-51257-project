#include "utils.h"

cl_program build_program(const cl_context ctx, const cl_device_id dev, const char* filename) {
   printf("Compiling %s\n", filename);

   char *program_buffer, *program_log;
   size_t program_size, log_size;
   int err;

   // Read the program file to a buffer
   FILE *program_handle;
   // The file has to be opened in rb mode for compatibility with CRLF line endings
   // https://stackoverflow.com/questions/34114963/opencl-program-build-error-source-file-is-not-valid-utf-8
   program_handle = fopen(filename, "rb");
   if(program_handle == NULL) {
      perror("Couldn't find the program file");
      exit(1);
   }
   fseek(program_handle, 0, SEEK_END);
   program_size = ftell(program_handle);
   rewind(program_handle);
   program_buffer = (char*) malloc(program_size + 1);
   program_buffer[program_size] = '\0';
   err = fread(program_buffer, sizeof(char), program_size, program_handle);
   if(err < 0) {
      perror("Couldn't read the program file");
      exit(1);
   }
   fclose(program_handle);

   // Create program from buffer
   cl_program program;
   program = clCreateProgramWithSource(ctx, 1,
      (const char**)&program_buffer, &program_size, &err);
   if(err < 0) {
      perror("Couldn't create the program");
      exit(1);
   }
   free(program_buffer);

   // Build program
   err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
   if(err < 0) {
      clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
      program_log = (char*) malloc(log_size + 1);
      program_log[log_size] = '\0';
      clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, log_size + 1, program_log, NULL);
      printf("%s\n", program_log);
      free(program_log);
      exit(1);
   }
   return program;
}

void check_status(const cl_int status) {
   if(status == CL_SUCCESS) {
      return;
   }
   printf("Got status code of an error: %d ", status);
   if(status == CL_INVALID_KERNEL) {
      printf("CL_INVALID_KERNEL\n");
   } else if(status == CL_INVALID_ARG_INDEX) {
      printf("CL_INVALID_ARG_INDEX\n");
   } else if(status == CL_INVALID_ARG_VALUE) {
      printf("CL_INVALID_ARG_VALUE\n");
   } else if(status == CL_INVALID_MEM_OBJECT) {
      printf("CL_INVALID_MEM_OBJECT\n");
   } else if(status == CL_INVALID_SAMPLER) {
      printf("CL_INVALID_SAMPLER\n");
   } else if(status == CL_INVALID_ARG_SIZE) {
      printf("CL_INVALID_ARG_SIZE\n");
   } else if(status == CL_INVALID_ARG_VALUE) {
      printf("CL_INVALID_ARG_VALUE\n");
   } else if(status == CL_OUT_OF_RESOURCES) {
      printf("CL_OUT_OF_RESOURCES\n");
   } else if(status == CL_OUT_OF_HOST_MEMORY) {
      printf("CL_OUT_OF_HOST_MEMORY\n");
   } else {
      printf("Unknown status code!\n");
   }
}

void enable_windows_console() {
   #ifdef _WIN32
   if(AttachConsole(ATTACH_PARENT_PROCESS) || AllocConsole()) {
      freopen("CONOUT$", "w", stdout);
      freopen("CONOUT$", "w", stderr);
   }
   #endif
}

void print_devices(const cl_platform_id platform) {
   cl_int status;
   char* name = clGetPlatformInfoStr(platform, CL_PLATFORM_NAME);
   printf("\nDevices of platform \"%s\"\n", name);
   free(name);
   
   cl_uint num_devices;
   status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, NULL, &num_devices);
   if(status) {
      printf("clGetDeviceIDs getting number failed: %d\n", status);
   }
   cl_device_id *devices = (cl_device_id*) malloc(
      num_devices * sizeof(cl_device_id)
   );
   status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, num_devices, devices, NULL);
   if(status) {
      printf("clGetDeviceIDs failed: %d\n", status);
   }

   for(int i = 0; i < num_devices; ++i) {
      const cl_device_id device = devices[i];

      char* name = clGetDeviceInfoStr(device, CL_DEVICE_NAME);
      printf("Name: %s\n", name);
      free(name);

      cl_ulong global_mem_cache_size;
      clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, sizeof(cl_ulong), &global_mem_cache_size, NULL);
      printf("- Global cache size: %lu\n", global_mem_cache_size);

      cl_ulong global_mem_size;
      clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), &global_mem_size, NULL);
      printf("- Global mem size: %lu\n", global_mem_size);

      cl_bool unified_memory;
      clGetDeviceInfo(device, CL_DEVICE_HOST_UNIFIED_MEMORY, sizeof(cl_bool), &unified_memory, NULL);
      printf("- Unified memory: %d\n", unified_memory);

      cl_uint max_compute_units;
      clGetDeviceInfo(device, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &max_compute_units, NULL);
      printf("- Max compute units: %u\n", max_compute_units);

      cl_uint max_constant_args;
      clGetDeviceInfo(device, CL_DEVICE_MAX_CONSTANT_ARGS, sizeof(cl_uint), &max_constant_args, NULL);
      printf("- Max constant args: %u\n", max_constant_args);

      size_t max_workgroup_size;
      clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &max_workgroup_size, NULL);
      printf("- Max workgroup size: %lu\n", max_workgroup_size);

      cl_device_type device_type;
      clGetDeviceInfo(device, CL_DEVICE_TYPE, sizeof(cl_device_type), &device_type, NULL);
      printf("- Device type:\n");
      if(device_type & CL_DEVICE_TYPE_CPU) {
         printf("  - CPU\n");
      }
      if(device_type & CL_DEVICE_TYPE_GPU) {
         printf("  - GPU\n");
      }
      if(device_type & CL_DEVICE_TYPE_ACCELERATOR) {
         printf("  - Accelerator\n");
      }
      if(device_type & CL_DEVICE_TYPE_DEFAULT) {
         printf("  - Default\n");
      }

      char* vendor = clGetDeviceInfoStr(device, CL_DEVICE_VENDOR);
      printf("- Vendor: %s\n", vendor);
      free(vendor);

      cl_uint vendor_id;
      clGetDeviceInfo(device, CL_DEVICE_VENDOR_ID, sizeof(cl_uint), &vendor_id, NULL);
      printf("- Vendor ID: %u\n", vendor_id);

      char* driver_version = clGetDeviceInfoStr(device, CL_DEVICE_VERSION);
      printf("- Driver version: %s\n", driver_version);
      free(driver_version);
   }
   free(devices);
   printf("\n");
}

char* clGetDeviceInfoStr(const cl_device_id device, const cl_device_info param_name) {
   cl_uint status;
   size_t size;
   status = clGetDeviceInfo(device, param_name, 0, NULL, &size);
   if(status) {
      printf("clGetDeviceInfo size query failed for %d: %d", param_name, status);
   }
   char* text = (char*) malloc(size);
   status = clGetDeviceInfo(device, param_name, size, text, NULL);
   if(status) {
      printf("clGetDeviceInfo failed for %d: %d", param_name, status);
   }
   return text;
}

char* clGetPlatformInfoStr(const cl_platform_id platform, const cl_platform_info param_name) {
   cl_uint status;
   size_t size;
   status = clGetPlatformInfo(platform, param_name, 0, NULL, &size);
   if(status) {
      printf("clGetPlatformInfo size query failed for %d: %d", param_name, status);
   }
   char* text = (char*) malloc(size);
   status = clGetPlatformInfo(platform, param_name, size, text, NULL);
   if(status) {
      printf("clGetPlatformInfo failed for %d: %d", param_name, status);
   }
   return text;
}
