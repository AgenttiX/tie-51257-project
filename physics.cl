__kernel
void physics(        
	__global float* restrict satellite_pos_x,
	__global float* restrict satellite_pos_y,
	__global float* restrict satellite_velocity_x,
    __global float* restrict satellite_velocity_y,
	const int window_width,
    const int window_height,
	const int satellite_count,
    const int physics_update_per_frame,
    const int delta_time)
	{
	
	const int i = get_global_id(0);

    // Definition of intermediate values
    double pos_x = satellite_pos_x[i];
    double pos_y = satellite_pos_y[i];
    double vel_x = satellite_velocity_x[i];
    double vel_y = satellite_velocity_y[i];
    const double center_h = window_width / 2;
    const double center_v = window_height / 2;

    // Physics iteration loop
    for(int physicsUpdateIndex = 0; 
       physicsUpdateIndex < physics_update_per_frame;
       ++physicsUpdateIndex){

        // Distance to the blackhole (bit ugly code because C-struct cannot have member functions)
        double positionToBlackHole_x = pos_x - center_h;
        double positionToBlackHole_y = pos_y - center_v;
        double distToBlackHoleSquared =
            positionToBlackHole_x * positionToBlackHole_x +
            positionToBlackHole_y * positionToBlackHole_y;
        double distToBlackHole = sqrt(distToBlackHoleSquared);

        // Gravity force
        double normalizedDirection_x = positionToBlackHole_x / distToBlackHole;
        double normalizedDirection_y = positionToBlackHole_y / distToBlackHole;
        double accumulation = 1.0 / distToBlackHoleSquared;

        // Delta time is used to make velocity same despite different FPS
        // Update velocity based on force
        vel_x -= accumulation * normalizedDirection_x *
            delta_time / physics_update_per_frame;
        vel_y -= accumulation * normalizedDirection_y *
            delta_time / physics_update_per_frame;

        // Update position based on velocity
        pos_x += vel_x * delta_time / physics_update_per_frame;
        pos_y += vel_y * delta_time / physics_update_per_frame;
    }
    satellite_pos_x[i] = pos_x;
    satellite_pos_y[i] = pos_y;
    satellite_velocity_x[i] = vel_x;
    satellite_velocity_y[i] = vel_y;
}
