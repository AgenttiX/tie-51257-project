#!/bin/sh
# Build script for Windows

SOURCE_FILE="${SOURCE_FILE:-parallel.c}"
echo "Building using ${SOURCE_FILE} as source"

CWD=$(pwd)
GCC="x86_64-w64-mingw32-gcc"
GLUT_INCLUDE="./temp/freeglut/include"
GLUT_LIB="./temp/freeglut/lib/x64"
OPENCL_INCLUDE="./temp/lightOCLSDK/include"
OPENCL_LIB="./temp/lightOCLSDK/lib/x86_64"

mkdir ${CWD}/temp -p
wget -nv https://www.transmissionzero.co.uk/files/software/development/GLUT/freeglut-MinGW.zip -O ./temp/freeglut-MinGW.zip
wget -nv https://github.com/GPUOpen-LibrariesAndSDKs/OCL-SDK/files/1406216/lightOCLSDK.zip -O ./temp/lightOCLSDK.zip
cd ${CWD}/temp
unzip ./freeglut-MinGW.zip
mkdir -p ${CWD}/temp/lightOCLSDK
cd ${CWD}/temp/lightOCLSDK
unzip ../lightOCLSDK.zip
cd ${CWD}

mkdir ./build -p

echo "Compiling with default configurations (but with -Wall)"
echo

echo "Compiling without optimizations"
${GCC} -c -o ./build/parallel_no-opt.o ${SOURCE_FILE} -D FREEGLUT_STATIC -I${GLUT_INCLUDE} -std=c99 -Wall
${GCC} -o ./build/parallel_no-opt.exe ./build/parallel_no-opt.o -lm -Wall -L${GLUT_LIB} -lfreeglut_static -lopengl32 -lwinmm -lgdi32 -Wl,--subsystem,windows
echo "Compiling without vectorization"
${GCC} -c -o ./build/parallel_O2.o ${SOURCE_FILE} -D FREEGLUT_STATIC -I${GLUT_INCLUDE} -std=c99 -Wall -O2
${GCC} -o ./build/parallel_O2.exe ./build/parallel_O2.o -lm -Wall -L${GLUT_LIB} -lfreeglut_static -lopengl32 -lwinmm -lgdi32 -Wl,--subsystem,windows
echo "Compiling with vectorization"
${GCC} -c -o ./build/parallel_vectorized.o ${SOURCE_FILE} -D FREEGLUT_STATIC -I${GLUT_INCLUDE} -std=c99 -Wall -O2 -ftree-vectorize -fopt-info-vec
${GCC} -o ./build/parallel_vectorized.exe ./build/parallel_vectorized.o -lm -Wall -L${GLUT_LIB} -lfreeglut_static -lopengl32 -lwinmm -lgdi32 -Wl,--subsystem,windows
echo "Compiling with relaxed math"
${GCC} -c -o ./build/parallel_relaxed.o ${SOURCE_FILE} -D FREEGLUT_STATIC -I${GLUT_INCLUDE} -std=c99 -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math
${GCC} -o ./build/parallel_relaxed.exe ./build/parallel_relaxed.o -lm -Wall -L${GLUT_LIB} -lfreeglut_static -lopengl32 -lwinmm -lgdi32 -Wl,--subsystem,windows
echo "Compiling with AVX2"
${GCC} -c -o ./build/parallel_avx2.o ${SOURCE_FILE} -D FREEGLUT_STATIC -I${GLUT_INCLUDE} -std=c99 -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -mavx2
${GCC} -o ./build/parallel_relaxed.exe ./build/parallel_relaxed.o -lm -Wall -L${GLUT_LIB} -lfreeglut_static -lopengl32 -lwinmm -lgdi32 -Wl,--subsystem,windows
echo "Compiling with OpenCL"
${GCC} -c -o ./build/parallel_opencl.o ${SOURCE_FILE} -D FREEGLUT_STATIC -I${GLUT_INCLUDE} -std=c99 -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -mavx2 -I${OPENCL_INCLUDE}
${GCC} -o ./build/parallel_opencl.exe ./build/parallel_opencl.o -lm -Wall -L${GLUT_LIB} -lfreeglut_static -lopengl32 -lwinmm -lgdi32 -Wl,--subsystem,windows -L${OPENCL_LIB} -lOpenCL

echo
echo "Compiling with own configurations"
echo

echo "Compiling for native microarchitecture"
${GCC} -c -o ./build/parallel_native.o ${SOURCE_FILE} -D FREEGLUT_STATIC -I${GLUT_INCLUDE} -std=c99 -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -march=native -mtune=native
${GCC} -o ./build/parallel_native.exe ./build/parallel_native.o -lm -Wall -L${GLUT_LIB} -lfreeglut_static -lopengl32 -lwinmm -lgdi32 -Wl,--subsystem,windows

echo "Compilation ready"
