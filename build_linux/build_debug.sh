#!/bin/sh
# Build script for Linux

SOURCE_FILE="${SOURCE_FILE:-parallel.c}"
echo "Building using ${SOURCE_FILE} as source"

mkdir ./build -p

echo "Compiling with OpenCL for debugging"
gcc -o ./build/parallel_debug ${SOURCE_FILE} -std=c11 -lglut -lGL -lm -O0 -g -ggdb -ftree-vectorize -fopt-info-vec -fopenmp -lOpenCL -Wall
