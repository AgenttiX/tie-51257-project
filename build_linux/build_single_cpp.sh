#!/bin/sh
# Build script for Linux

SOURCE_FILE="${SOURCE_FILE:-parallel.cpp}"
echo "Building using ${SOURCE_FILE} as source"

mkdir ./build -p

echo "Compiling with OpenCL for native microarchitecture (without OpenMP & OpenCL)"
g++ -o ./build/parallel_native ${SOURCE_FILE} -std=c++17 -lglut -lGL -lm -O2 -ftree-vectorize -fopt-info-vec -ffast-math -march=native -mtune=native -Wall
