#!/bin/sh
# Build script for Linux

SOURCE_FILE="${SOURCE_FILE:-parallel.c}"
echo "Building using ${SOURCE_FILE} as source"

mkdir ./build -p

echo "Compiling with default configurations (but with -Wall)"
echo

echo "Compiling without optimizations"
gcc -o ./build/parallel_no-opt ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall
echo "Compiling without vectorization"
gcc -o ./build/parallel_O2 ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2
echo "Compiling with vectorization"
gcc -o ./build/parallel_vectorized ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec
echo "Compiling with relaxed math"
gcc -o ./build/parallel_relaxed ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math
echo "Compiling with AVX2"
gcc -o ./build/parallel_avx2 ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -mavx2
echo "Compiling with OpenMP"
gcc -o ./build/parallel_openmp ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -mavx2 -fopenmp
echo "Compiling with OpenCL"
gcc -o ./build/parallel_opencl ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -mavx2 -fopenmp -lOpenCL

echo
echo "Compiling with own configurations"
echo

# Without OpenCL

echo "Compiling for native microarchitecture"
gcc -o ./build/parallel_native ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp -march=native -mtune=native

echo "Compiling for native microarchitecture with O3"
gcc -o ./build/parallel_native_O3 ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O3 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp -march=native -mtune=native

echo "Compiling for native microarchitecture with Ofast"
# -Ofast includes -O3, -ffast-math, -fno-protect-parens and -fstack-arrays
gcc -o ./build/parallel_native_Ofast ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -Ofast -fopt-info-vec -fopenmp -march=native -mtune=native

echo "Compiling for CPUs without AVX"
gcc -o ./build/parallel_no-avx ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp -mmmx -msse -msse2 -msse3 -mssse3 -msse4.1 -msse4.2 -msse4

# With OpenCL

echo "Compiling with OpenCL for native microarchitecture"
gcc -o ./build/parallel_opencl_native ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp -lOpenCL -march=native -mtune=native

echo "Compiling with OpenCL for native microarchitecture with O3"
gcc -o ./build/parallel_opencl_native_O3 ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O3 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp -lOpenCL -march=native -mtune=native

echo "Compiling with OpenCL for CPUs without AVX"
gcc -o ./build/parallel_opencl_no-avx ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp -lOpenCL -mmmx -msse -msse2 -msse3 -mssse3 -msse4.1 -msse4.2 -msse4

echo "Compiling with OpenCL for native microarchitecture with Ofast"
# -Ofast includes -O3, -ffast-math, -fno-protect-parens and -fstack-arrays
gcc -o ./build/parallel_opencl_native_Ofast ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -Ofast -fopt-info-vec -fopenmp -lOpenCL -march=native -mtune=native

# Debugging

echo "Compiling for valgrind"
gcc -o ./build/parallel_valgrind ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O0 -g -ftree-vectorize -fopt-info-vec -fopenmp -march=native -mtune=native

echo "Compiling for gprof"
gcc -o ./build/parallel_gprof ${SOURCE_FILE} -std=c99 -g -pg -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp -march=native -mtune=native

echo "Compiling for callgrind"
gcc -o ./build/parallel_callgrind ${SOURCE_FILE} -std=c99 -g -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp -march=native -mtune=native

echo "Compilation ready"
