#!/bin/sh
# Build script for Linux

SOURCE_FILE="${SOURCE_FILE:-parallel.c}"
echo "Building using ${SOURCE_FILE} as source"

mkdir ./build -p

echo "Compiling with OpenCL for native microarchitecture"
gcc -o ./build/parallel_native ${SOURCE_FILE} -std=c11 -lglut -lGL -lm -O2 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp -lOpenCL -march=native -mtune=native -Wall
