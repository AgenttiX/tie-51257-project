/* TIE-51257 Parallelization Excercise 2019
   Copyright (c) 2016 Matias Koskela matias.koskela@tut.fi
                      Heikki Kultala heikki.kultala@tut.fi

VERSION 1.1 - updated to not have stuck satellites so easily
VERSION 1.2 - updated to not have stuck satellites hopefully at all.
VERSION 19.0 - make all satelites affect the color with weighted average.
               add physic correctness check.
*/

// Example compilation on linux
// no optimization:   gcc -o parallel parallel.c -std=c99 -lglut -lGL -lm
// most optimizations: gcc -o parallel parallel.c -std=c99 -lglut -lGL -lm -O2
// +vectorization +vectorize-infos: gcc -o parallel parallel.c -std=c99 -lglut -lGL -lm -O2 -ftree-vectorize -fopt-info-vec
// +math relaxation:  gcc -o parallel parallel.c -std=c99 -lglut -lGL -lm -O2 -ftree-vectorize -fopt-info-vec -ffast-math
// prev and OpenMP:   gcc -o parallel parallel.c -std=c99 -lglut -lGL -lm -O2 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp
// prev and OpenCL:   gcc -o parallel parallel.c -std=c99 -lglut -lGL -lm -O2 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp -lOpenCL

// Example compilation on macos X
// no optimization:   gcc -o parallel parallel.c -std=c99 -framework GLUT -framework OpenGL
// most optimization: gcc -o parallel parallel.c -std=c99 -framework GLUT -framework OpenGL -O3



#ifdef _WIN32
#include <windows.h>
#endif
#include <stdio.h> // printf
#include <math.h> // INFINITY
#include <stdlib.h>
#include <string.h>

// Window handling includes
#ifndef __APPLE__
#include <GL/gl.h>
#include <GL/glut.h>
#else
#include <OpenGL/gl.h>
#include <GLUT/glut.h>
#endif

// Custom includes
// stdint.h is required for MSVC to find uint64_t
#include <stdint.h>
#define CL_TARGET_OPENCL_VERSION 220
// This is required to prevent deprecation warning of clCreateCommandQueue
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
// By including the .c directly we don't have to compile it separately
#include "utils.c"
#include <CL/cl.h>

// These are used to decide the window size
#define WINDOW_HEIGHT 1024
#define WINDOW_WIDTH  1024

// The number of satelites can be changed to see how it affects performance.
// Benchmarks must be run with the original number of satellites
#define SATELITE_COUNT 64

// These are used to control the satelite movement
#define SATELITE_RADIUS 3.16f
#define MAX_VELOCITY 0.1f
// It appears that we encountered the limitations for the number of OpenCL arguments,
// so this is hardcoded in the OpenCL version.
// Therefore please do not change this value!
#define GRAVITY 1.0f
#define DELTATIME 32
#define PHYSICSUPDATESPERFRAME 100000

// Some helpers to window size variables
#define SIZE WINDOW_HEIGHT*WINDOW_HEIGHT
#define HORIZONTAL_CENTER (WINDOW_WIDTH / 2)
#define VERTICAL_CENTER (WINDOW_HEIGHT / 2)

// Is used to find out frame times
int previousFrameTimeSinceStart = 0;
int previousFinishTime = 0;
unsigned int frameNumber = 0;
unsigned int seed = 0;

// Stores 2D data like the coordinates
typedef struct{
   float x;
   float y;
} floatvector;

// Stores 2D data like the coordinates
typedef struct{
   double x;
   double y;
} doublevector;

// Stores rendered colors. Each float may vary from 0.0f ... 1.0f
typedef struct{
   float red;
   float green;
   float blue;
} color;

// Stores the satelite data, which fly around black hole in the space
typedef struct{
   color identifier;
   floatvector position;
   floatvector velocity;
} satelite;

// Pixel buffer which is rendered to the screen
color* pixels;

// Pixel buffer which is used for error checking
color* correctPixels;

// Buffer for all satelites in the space
satelite* satelites;
satelite* backupSatelites;








// ## You may add your own variables here ##
#define DBG_ENABLE 0
#define GPU_PHYSICS 0
// #define WG_SIZE (1*1)
// #define WG_SIZE (2*2)
// #define WG_SIZE (4*4)
// #define WG_SIZE (8*4)
// #define WG_SIZE (8*8)
// #define WG_SIZE (16*16)
// #define WG_SIZE (32*32)
// #define WG_SIZE (64*64)  // This doesn't work on Nvidia MX150

#define NUM_PIXELS (WINDOW_HEIGHT*WINDOW_WIDTH)
#ifdef _WIN32
   #define restrict __restrict
#endif

void initBuffers();
static inline cl_event run_physics_kernel(cl_mem *buf_pos_x, cl_mem *buf_pos_y, cl_mem *buf_vel_x, cl_mem *buf_vel_y);

cl_command_queue graphics_queue;
cl_command_queue physics_queue;
cl_context graphics_context;
cl_context physics_context;

cl_kernel graphics_kernel;
cl_kernel physics_kernel;
cl_program graphics_program;
cl_program physics_program;

cl_mem buf_pixels;
cl_mem buf_satellite_pos_x;
cl_mem buf_satellite_pos_y;
cl_mem buf_satellite_pos_x_graphics;
cl_mem buf_satellite_pos_y_graphics;
cl_mem buf_satellite_vel_x;
cl_mem buf_satellite_vel_y;
cl_mem buf_satellite_colors;

cl_event graphics_kernel_event;
cl_event physics_kernel_event;

// ## You may add your own initialization routines here ##

void init(){
   // Enable writing to console on Windows
   // https://stackoverflow.com/questions/54536/win32-gui-app-that-writes-usage-text-to-stdout-when-invoked-as-app-exe-help
   enable_windows_console();
   printf("Initializing\n");

   #ifdef WG_SIZE
      printf("Graphics workgroup size: %d\n", WG_SIZE);
   #else
      printf("Graphics workgroup size: automatic\n");
   #endif

   cl_int status;

   // Number of platforms
   cl_uint numPlatforms = 0;
   status = clGetPlatformIDs(0, NULL, &numPlatforms);
   if (status) {
      printf("OpenCL initialization failed: %d\n", status);
   }
   printf("Found %d OpenCL platforms\n", numPlatforms);

   // Allocate space for the platform array
   cl_platform_id *platforms = NULL;
   platforms = (cl_platform_id*) malloc(
      numPlatforms * sizeof(cl_platform_id)
   );

   // Get the platform data
   status = clGetPlatformIDs(numPlatforms, platforms, NULL);
   if (status) {
      printf("Failed to get platform IDs: %d\n", status);
   }

   const char* names[5] = {"Profile", "Version", "Name", "Vendor", "Extensions"};
   const cl_platform_info inds[5] = {
      CL_PLATFORM_PROFILE,
      CL_PLATFORM_VERSION,
      CL_PLATFORM_NAME,
      CL_PLATFORM_VENDOR,
      CL_PLATFORM_EXTENSIONS
   };

   int amd_platform_ind = -1;
   int intel_platform_ind = -1;
   int nvidia_platform_ind = -1;
   int pocl_platform_ind = -1;

   // Print platform info
   for(int plat_i = 0; plat_i < numPlatforms; ++plat_i) {
      printf("Platform %d:\n", plat_i);
      uint64_t plat_id_for_print = (uint64_t) platforms[plat_i];
      printf("- ID: %lu\n", plat_id_for_print);
      char *text;
      for(int field_i = 0; field_i < 5; ++field_i) {
         size_t size;
         clGetPlatformInfo(platforms[plat_i], inds[field_i], 0, NULL, &size);
         text = (char*) malloc(size);
         clGetPlatformInfo(platforms[plat_i], inds[field_i], size, text, NULL);
         printf("- %s: %s\n", names[field_i], text);

         if(field_i == 2) {
            if(!strcmp(text, "AMD Accelerated Parallel Processing")) {
               amd_platform_ind = plat_i;
            } else if(!strcmp(text, "Intel(R) OpenCL")) {
               intel_platform_ind = plat_i;
            } else if(!strcmp(text, "Intel(R) CPU Runtime for OpenCL(TM) Applications")) {
               intel_platform_ind = plat_i;
            } else if(!strcmp(text, "NVIDIA CUDA")) {
               nvidia_platform_ind = plat_i;
            } else if(!strcmp(text, "Portable Computing Language")) {
               pocl_platform_ind = plat_i;
            } else {
               printf("   - Unknown name! (%s)\n", text);
            }
         }
         free(text);
      }
   }
   printf("\nAMD platform index: %d\n", amd_platform_ind);
   printf("Intel platform index: %d\n", intel_platform_ind);
   printf("Nvidia platform index: %d\n", nvidia_platform_ind);
   printf("POCL platform index: %d\n", pocl_platform_ind);

   cl_uint physics_platform_ind = -1;
   cl_uint graphics_platform_ind = -1;
   if(amd_platform_ind != -1) {
      graphics_platform_ind = amd_platform_ind;
   } else if(nvidia_platform_ind != -1) {
      graphics_platform_ind = nvidia_platform_ind;
   } else if (intel_platform_ind != -1) {
      graphics_platform_ind = intel_platform_ind;
   } else {
      graphics_platform_ind = 0;
   }

   //  physics_platform_ind = graphics_platform_ind;
   if(intel_platform_ind != -1) {
      physics_platform_ind = intel_platform_ind;
   } else if (pocl_platform_ind != -1) {
      physics_platform_ind = pocl_platform_ind;
   } else {
      physics_platform_ind = 0;
   }
   #if GPU_PHYSICS
      physics_platform_ind = graphics_platform_ind;
   #endif
   printf("Physics platform index: %d\n", physics_platform_ind);
   printf("Graphics platform index: %d\n", graphics_platform_ind);

   for(int i = 0; i < numPlatforms; ++i) {
      print_devices(platforms[i]);
   }

   // Create array for the number of devices of each platform
   // cl_uint *numDevices = NULL;
   // numDevices = (cl_uint*) malloc(
   //    numPlatforms * sizeof(cl_uint)
   // );

   // Get the number of devices for each platform
   // cl_uint totalDevices = 0;
   // for(cl_uint plat_i = 0; plat_i < numPlatforms; ++plat_i) {
   //    cl_uint numDevicesForThis = 0;
   //    clGetDeviceIDs(platforms[plat_i], CL_DEVICE_TYPE_ALL, 0, NULL, &numDevicesForThis);
   //    printf("Platform %d has %d devices\n", plat_i, numDevicesForThis);
   //    totalDevices += numDevicesForThis;
   //    numDevices[plat_i] = numDevicesForThis;
   // }

   cl_device_id graphics_dev;
   status = clGetDeviceIDs(platforms[graphics_platform_ind], CL_DEVICE_TYPE_ALL, 1, &graphics_dev, NULL);
   if(status) {
      printf("Getting graphics device failed: %d", status);
   }
   cl_device_id physics_dev;
   #if GPU_PHYSICS
      physics_dev = graphics_dev;
   #else
      status = clGetDeviceIDs(platforms[physics_platform_ind], CL_DEVICE_TYPE_CPU, 1, &physics_dev, NULL);
      if(status) {
         printf("Getting physics device failed: %d", status);
      }
   #endif
   free(platforms);

   char* physics_dev_name = clGetDeviceInfoStr(physics_dev, CL_DEVICE_NAME);
   printf("Physics device: %s\n", physics_dev_name);
   char* graphics_dev_name = clGetDeviceInfoStr(graphics_dev, CL_DEVICE_NAME);
   printf("Graphics decice: %s\n", graphics_dev_name);

   printf("Creating contexts\n");
   physics_context = clCreateContext(
      NULL, 1,
      &physics_dev, NULL, NULL, &status);
   if(status) {
      printf("Physics context creation failed: %d\n", status);
   }
   graphics_context = clCreateContext(
      NULL, 1,
      &graphics_dev, NULL, NULL, &status);
   if(status) {
      printf("Graphics context creation failed: %d\n", status);
   }
   // free(devices);
   // free(numDevices);

   printf("Creating command queues\n");
   physics_queue = clCreateCommandQueue(physics_context, physics_dev, CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, &status);
   if(status) {
      printf("Physics queue creation failed: %d", status);
   }
   graphics_queue = clCreateCommandQueue(graphics_context, graphics_dev, CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, &status);
   if(status) {
      printf("Graphics queue creation failed: %d", status);
   }

   printf("Building programs\n");
   physics_program = build_program(physics_context, physics_dev, "physics.cl");
   graphics_program = build_program(graphics_context, graphics_dev, "graphics.cl");
   
   physics_kernel = clCreateKernel(physics_program, "physics", &status);
   if(status) {
      printf("Physics kernel creation failed: %d\n", status);
   }
   graphics_kernel = clCreateKernel(graphics_program, "graphics", &status);
   if(status) {
      printf("Graphics kernel creation failed: %d\n", status);
   }

   printf("Initializing buffers\n");
   initBuffers();

   printf("Enqueuing physics kernel\n");
   physics_kernel_event = run_physics_kernel(
      &buf_satellite_pos_x, &buf_satellite_pos_y,
      &buf_satellite_vel_x, &buf_satellite_vel_y);

   printf("Initialization ready\n\n");
}

void initBuffers(){
   cl_int status;

   // Pixels
   buf_pixels = clCreateBuffer(
      graphics_context,
      CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY,
      3*sizeof(float)*NUM_PIXELS, NULL, &status);
   if(status) {
      printf("Creating buf_pixels failed");
   }

   // Satellite colors
   float arr_sat_colors[SATELITE_COUNT*3];
   for(int i = 0; i < SATELITE_COUNT; ++i) {
      const int i3 = 3*i;
      arr_sat_colors[i3] = satelites[i].identifier.red;
      arr_sat_colors[i3+1] = satelites[i].identifier.green;
      arr_sat_colors[i3+2] = satelites[i].identifier.blue;
   }
   buf_satellite_colors = clCreateBuffer(
      graphics_context,
      CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR,
      3*sizeof(float)*SATELITE_COUNT, &arr_sat_colors, &status);
   if(status) {
      printf("Creating buf_satellite_colors failed");
   }

   // Satellite positions and velocities
   float arr_pos_x[SATELITE_COUNT];
   float arr_pos_y[SATELITE_COUNT];
   float arr_vel_x[SATELITE_COUNT];
   float arr_vel_y[SATELITE_COUNT];
   for(int i = 0; i < SATELITE_COUNT; ++i) {
      arr_pos_x[i] = satelites[i].position.x;
      arr_pos_y[i] = satelites[i].position.y;
      arr_vel_x[i] = satelites[i].velocity.x;
      arr_vel_y[i] = satelites[i].velocity.y;
   }

   // Physics buffers
   buf_satellite_pos_x = clCreateBuffer(
      physics_context,
      CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR,
      sizeof(float)*SATELITE_COUNT, &arr_pos_x, &status);
   if(status) {
      printf("Creating buf_satellite_pos_x failed");
   }
   buf_satellite_pos_y = clCreateBuffer(
      physics_context,
      CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR,
      sizeof(float)*SATELITE_COUNT, &arr_pos_y, &status);
   if(status) {
      printf("Creating buf_satellite_pos_y failed");
   }
   buf_satellite_vel_x = clCreateBuffer(
      physics_context,
      CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR,
      sizeof(float)*SATELITE_COUNT, &arr_vel_x, &status);
     if(status) {
      printf("Creating buf_satellite_vel_x failed");
   }
   buf_satellite_vel_y = clCreateBuffer(
      physics_context,
      CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR,
      sizeof(float)*SATELITE_COUNT, &arr_vel_y, &status);
   if(status) {
      printf("Creating buf_satellite_vel_y failed");
   }

   // Satellite buffers for graphics
   buf_satellite_pos_x_graphics = clCreateBuffer(
      graphics_context,
      CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY,
      sizeof(float)*SATELITE_COUNT, NULL, &status);
   if(status) {
      printf("Creating buf_satellite_pos_x_graphics failed");
   }
   buf_satellite_pos_y_graphics = clCreateBuffer(
      graphics_context,
      CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY,
      sizeof(float)*SATELITE_COUNT, NULL, &status);
   if(status) {
      printf("Creating buf_satellite_pos_y_graphics failed");
   }

   printf("Initialization ready\n\n");
}


void copy_physics_to_host(
   const cl_event *kernel_event,
   const cl_mem buf_pos_x, const cl_mem buf_pos_y,
   const cl_mem buf_vel_x, const cl_mem buf_vel_y) {
   cl_int status;
   cl_event pos_x_event;
   cl_event pos_y_event;
   cl_event vel_x_event;
   cl_event vel_y_event;
   float arr_pos_x[SATELITE_COUNT];
   float arr_pos_y[SATELITE_COUNT];
   float arr_vel_x[SATELITE_COUNT];
   float arr_vel_y[SATELITE_COUNT];
   status = clEnqueueReadBuffer(physics_queue, buf_pos_x, CL_FALSE, 0,
      sizeof(float)*SATELITE_COUNT, &arr_pos_x, 1, kernel_event, &pos_x_event);
   #if DBG_ENABLE
      if(status) {
         printf("read status position_x: %d\n", status);
      }
   #endif
   status = clEnqueueReadBuffer(physics_queue, buf_pos_y, CL_FALSE, 0,
      sizeof(float)*SATELITE_COUNT, &arr_pos_y, 1, kernel_event, &pos_y_event);
   #if DBG_ENABLE
      if(status) {
         printf("read status position_y: %d\n", status);
      }
   #endif
   status = clEnqueueReadBuffer(physics_queue, buf_vel_x, CL_FALSE, 0,
      sizeof(float)*SATELITE_COUNT, &arr_vel_x, 1, kernel_event, &vel_x_event);
   #if DBG_ENABLE
      if(status) {
         printf("read status velocity_x: %d\n", status);
      }
   #endif
   status = clEnqueueReadBuffer(physics_queue, buf_vel_y, CL_FALSE, 0,
      sizeof(float)*SATELITE_COUNT, &arr_vel_y, 1, kernel_event, &vel_y_event);
   #if DBG_ENABLE
      if(status) {
         printf("read status velocity_y: %d\n", status);
      }
   #endif
   const cl_event read_events[4] = {pos_x_event, pos_y_event, vel_x_event, vel_y_event};
   clWaitForEvents(4, read_events);

   // Copy to course memory for error checking
   for (int i = 0; i < SATELITE_COUNT; ++i) {
      satelites[i].position.x = arr_pos_x[i];
      satelites[i].position.y = arr_pos_y[i];
      satelites[i].velocity.x = arr_vel_x[i];
      satelites[i].velocity.y = arr_vel_y[i];
   }
}


static inline cl_event run_physics_kernel(
   cl_mem* restrict buf_pos_x, cl_mem* restrict buf_pos_y,
   cl_mem* restrict buf_vel_x, cl_mem* restrict buf_vel_y) {
   const int window_width = WINDOW_WIDTH;
   const int window_height = WINDOW_HEIGHT;
   const int satelite_count = SATELITE_COUNT;
   const int physics_update_per_frame = PHYSICSUPDATESPERFRAME;
   const int deltatime = DELTATIME;
   // const float gravity = GRAVITY;

   cl_int status;
   status = clSetKernelArg(physics_kernel, 0, sizeof(cl_mem), buf_pos_x);
   #if DBG_ENABLE
      check_status(status);
   #endif
	status = clSetKernelArg(physics_kernel, 1, sizeof(cl_mem), buf_pos_y);
	#if DBG_ENABLE
      check_status(status);
   #endif
   status = clSetKernelArg(physics_kernel, 2, sizeof(cl_mem), buf_vel_x);
   #if DBG_ENABLE
      check_status(status);
   #endif
   status = clSetKernelArg(physics_kernel, 3, sizeof(cl_mem), buf_vel_y);
   #if DBG_ENABLE
      check_status(status);
   #endif
	status = clSetKernelArg(physics_kernel, 4, sizeof(int), &window_width);
   #if DBG_ENABLE
      check_status(status);
   #endif
   status = clSetKernelArg(physics_kernel, 5, sizeof(int), &window_height);
   #if DBG_ENABLE
      check_status(status);
   #endif
   status = clSetKernelArg(physics_kernel, 6, sizeof(int), &satelite_count);
   #if DBG_ENABLE
      check_status(status);
   #endif
   status = clSetKernelArg(physics_kernel, 7, sizeof(int), &physics_update_per_frame);
   #if DBG_ENABLE
      check_status(status);
   #endif
   status = clSetKernelArg(physics_kernel, 8, sizeof(int), &deltatime);
   #if DBG_ENABLE
      check_status(status);
   #endif
   // It appears that we encountered the OpenCL limitations for the number of argument
   // status = clSetKernelArg(physics_kernel, 9, sizeof(float), &gravity);
   // #if DBG_ENABLE
   //    check_status(status);
   // #endif

	const size_t globalWorkSize[1] = {SATELITE_COUNT};

   cl_event kernel_event;
	status = clEnqueueNDRangeKernel(physics_queue, physics_kernel, 1, NULL,
		globalWorkSize, NULL, 0, NULL,  &kernel_event);
	#if DBG_ENABLE
      if(status) {
         printf("kernel status: %d\n", status);
      }
   #endif
   return kernel_event;
}


static inline void copy_to_graphics(
   const cl_event *event,
   const cl_mem buf_x, const cl_mem buf_y,
   cl_mem buf_x_graphics, cl_mem buf_y_graphics) {
   
   cl_int status;

   // This does not work since the devices are in different contexts (error -38)
   // cl_event copy_x_event;
   // cl_event copy_y_event;
   // status = clEnqueueCopyBuffer(
   //    physics_queue,
   //    *buf_x, *buf_x_graphics,
   //    0, 0, sizeof(float)*SATELITE_COUNT, 1, event, &copy_x_event);
   // printf("Satellite buf_x copy failed: %d\n", status);
   // status = clEnqueueCopyBuffer(
   //    physics_queue,
   //    *buf_y, *buf_y_graphics,
   //    0, 0, sizeof(float)*SATELITE_COUNT, 1, event, &copy_y_event);
   // printf("Satellite buf_y copy failed: %d\n", status);

   cl_event map_x_event;
   cl_event map_y_event;
   float* arr_x = (float*) clEnqueueMapBuffer(
      physics_queue, buf_x, CL_FALSE, CL_MAP_READ, 0, sizeof(float)*SATELITE_COUNT, 1, event, &map_x_event, &status);
   #if DBG_ENABLE
      if(status) {
         printf("clEnqueueMapBuffer for buf_x failed: %d\n", status);
      }
   #endif
   float* arr_y = (float*) clEnqueueMapBuffer(
      physics_queue, buf_y, CL_FALSE, CL_MAP_READ, 0, sizeof(float)*SATELITE_COUNT, 1, event, &map_y_event, &status);
   #if DBG_ENABLE
      if(status) {
         printf("clEnqueueMapBuffer for buf_y failed: %d\n", status);
      }
   #endif
   const cl_event map_events[2] = {map_x_event, map_y_event};
   clWaitForEvents(2, map_events);
   cl_event write_x_event;
   cl_event write_y_event;
   status = clEnqueueWriteBuffer(
      graphics_queue, buf_x_graphics, CL_FALSE, 0, sizeof(float)*SATELITE_COUNT, arr_x, 0, NULL, &write_x_event);
   #if DBG_ENABLE
      if(status) {
         printf("clEnqueueWriteBuffer for buf_x failed: %d\n", status);
      }
   #endif
   status = clEnqueueWriteBuffer(
      graphics_queue, buf_y_graphics, CL_FALSE, 0, sizeof(float)*SATELITE_COUNT, arr_y, 0, NULL, &write_y_event);
   #if DBG_ENABLE
      if(status) {
         printf("clEnqueueWriteBuffer for buf_y failed: %d\n", status);
      }
   #endif
   const cl_event write_events[2] = {write_x_event, write_y_event};
   clWaitForEvents(2, write_events);
   clEnqueueUnmapMemObject(physics_queue, buf_x, arr_x, 0, NULL, &map_x_event);
   clEnqueueUnmapMemObject(physics_queue, buf_y, arr_y, 0, NULL, &map_y_event);
   const cl_event unmap_events[2] = {map_x_event, map_y_event};
   clWaitForEvents(2, unmap_events);
}

// ## You are asked to make this code parallel ##
// Physics engine loop. (This is called once a frame before graphics engine) 
// Moves the satelites based on gravity
// This is done multiple times in a frame because the Euler integration 
// is not accurate enough to be done only once
void parallelPhysicsEngine(){
   #if DBG_ENABLE
      printf("Starting physics\n");
   #endif

   if(frameNumber > 0) {
      clWaitForEvents(1, &graphics_kernel_event);
   }
   copy_to_graphics(
      &physics_kernel_event,
      buf_satellite_pos_x, buf_satellite_pos_y,
      buf_satellite_pos_x_graphics, buf_satellite_pos_y_graphics);

   if(frameNumber < 2) {
      #if DBG_ENABLE
         printf("Copying physics results for checking\n");
      #endif
      copy_physics_to_host(
         &physics_kernel_event,
         buf_satellite_pos_x, buf_satellite_pos_y,
         buf_satellite_vel_x, buf_satellite_vel_y);
   } else {
      // clWaitForEvents(1, &kernel_event);
   }

   physics_kernel_event = run_physics_kernel(
      &buf_satellite_pos_x, &buf_satellite_pos_y,
      &buf_satellite_vel_x, &buf_satellite_vel_y);

   #if DBG_ENABLE
      printf("Physics ready\n");
   #endif
}


float arr_pixels[3*NUM_PIXELS];

// Using the restrict keyword here enables vectorization
static inline void copy_pixels(color* restrict pixels_color, float* restrict pixels_arr) {
   for(int i = 0; i < NUM_PIXELS; ++i) {
      const int i3 = 3*i;
      pixels_color[i].red = pixels_arr[i3];
      pixels_color[i].green = pixels_arr[i3+1];
      pixels_color[i].blue = pixels_arr[i3+2];
   }
}

void sequentialGraphicsEngine();

void compare_pixels() {
   // This has to be called here as well to fill correctPixels
   sequentialGraphicsEngine();
   double err_abs = 0.0;
   double err_sq = 0.0;
   double err_sq_r = 0.0;
   double err_sq_g = 0.0;
   double err_sq_b = 0.0;
   for(int i = 0; i < NUM_PIXELS; ++i) {
      const double r_diff = pixels[i].red - correctPixels[i].red;
      const double g_diff = pixels[i].green - correctPixels[i].green;
      const double b_diff = pixels[i].blue - correctPixels[i].blue;
      if(r_diff < 0) {
         err_abs -= r_diff;
      } else {
         err_abs += r_diff;
      }
      if(g_diff < 0) {
         err_abs -= g_diff;
      } else {
         err_abs += g_diff;
      }
      if(b_diff < 0) {
         err_abs -= b_diff;
      } else {
         err_abs += b_diff;
      }
      err_sq_r += r_diff*r_diff;
      err_sq_g += g_diff*g_diff;
      err_sq_b += b_diff*b_diff;
      err_sq += r_diff*r_diff + g_diff*g_diff + b_diff*b_diff;
   }
   printf("Pixels total abs error: %f\n", err_abs);
   printf("Pixels total squared error: %f\n", err_sq);
   printf("Pixels MSE:   %f\n", err_sq / NUM_PIXELS);
   printf("Pixels MSE r: %f\n", err_sq_r / NUM_PIXELS);
   printf("Pixels MSE g: %f\n", err_sq_g / NUM_PIXELS);
   printf("Pixels MSE b: %f\n", err_sq_b / NUM_PIXELS);
   printf("Correct  pixel 0: (%.9f, %.9f, %.9f)\n", correctPixels[0].red, correctPixels[0].green, correctPixels[0].blue);
   printf("Computed pixel 0: (%.9f, %.9f, %.9f)\n", pixels[0].red, pixels[0].green, pixels[0].blue);
   printf("Correct  pixel 1: (%.9f, %.9f, %.9f)\n", correctPixels[1].red, correctPixels[1].green, correctPixels[1].blue);
   printf("Computed pixel 1: (%.9f, %.9f, %.9f)\n", pixels[1].red, pixels[1].green, pixels[1].blue);
}

static inline cl_event run_graphics_kernel(
   cl_mem* buf_pixels,
   cl_mem* buf_pos_x, cl_mem* buf_pos_y) {

   cl_int status;

   const cl_int window_width = WINDOW_WIDTH;
   const cl_int satelite_count = SATELITE_COUNT;
   const cl_float satelite_radius = SATELITE_RADIUS;

   status = clSetKernelArg(graphics_kernel, 0, sizeof(cl_mem), buf_pixels);
   #if DBG_ENABLE
      check_status(status);
   #endif
   status = clSetKernelArg(graphics_kernel, 1, sizeof(cl_mem), &buf_satellite_colors);
   #if DBG_ENABLE
      check_status(status);
   #endif
   status = clSetKernelArg(graphics_kernel, 2, sizeof(cl_mem), buf_pos_x);
   #if DBG_ENABLE
      check_status(status);
   #endif
   status = clSetKernelArg(graphics_kernel, 3, sizeof(cl_mem), buf_pos_y);
   #if DBG_ENABLE
      check_status(status);
   #endif
   status = clSetKernelArg(graphics_kernel, 4, sizeof(cl_int), &window_width);
   #if DBG_ENABLE
      check_status(status);
   #endif
   status = clSetKernelArg(graphics_kernel, 5, sizeof(cl_int), &satelite_count);
   #if DBG_ENABLE
      check_status(status);
   #endif
   status = clSetKernelArg(graphics_kernel, 6, sizeof(cl_float), &satelite_radius);
   #if DBG_ENABLE
      check_status(status);
   #endif

   const size_t globalWorkSize[1] = {NUM_PIXELS};
   #ifdef WG_SIZE
      const size_t localWorkSize[1] = {WG_SIZE};
   #else
      const size_t* localWorkSize = NULL;
   #endif
   cl_event kernel_event;
   status = clEnqueueNDRangeKernel(graphics_queue, graphics_kernel, 1, NULL,
      globalWorkSize, localWorkSize, 0, NULL, &kernel_event);
   #if DBG_ENABLE
      if(status) {
         printf("kernel status: %d\n", status);
      }
   #endif
   return kernel_event;
}

// ## You are asked to make this code parallel ##
// Rendering loop (This is called once a frame after physics engine) 
// Decides the color for each pixel.
void parallelGraphicsEngine(){
   #if DBG_ENABLE
      printf("Starting graphics\n");
   #endif

   // Todo: It might run with the first satellite positions twice on the third frame or something like that
   if(frameNumber < 2) {
      graphics_kernel_event = run_graphics_kernel(
         &buf_pixels, &buf_satellite_pos_x_graphics, &buf_satellite_pos_y_graphics);
   }
   
   cl_int status;
   status = clEnqueueReadBuffer(graphics_queue, buf_pixels, CL_TRUE, 0,
      3*sizeof(float)*NUM_PIXELS, arr_pixels, 1, &graphics_kernel_event, NULL);
   #if DBG_ENABLE
      if(status) {
         printf("read status: %d\n", status);
      }
   #endif

   graphics_kernel_event = run_graphics_kernel(
      &buf_pixels, &buf_satellite_pos_x_graphics, &buf_satellite_pos_y_graphics);

   copy_pixels(pixels, arr_pixels);
   #if DBG_ENABLE
      if(frameNumber < 2) {
         compare_pixels();
      }
      printf("Graphics ready\n");
   #endif
}

// ## You may add your own destrcution routines here ##
void destroy(){
   clReleaseKernel(graphics_kernel);
   clReleaseKernel(physics_kernel);
   clReleaseProgram(graphics_program);
   clReleaseProgram(physics_program);
   clReleaseCommandQueue(graphics_queue);
   clReleaseCommandQueue(physics_queue);

   clReleaseMemObject(buf_pixels);
   clReleaseMemObject(buf_satellite_colors);
   clReleaseMemObject(buf_satellite_pos_x);
   clReleaseMemObject(buf_satellite_pos_y);
   clReleaseMemObject(buf_satellite_pos_x_graphics);
   clReleaseMemObject(buf_satellite_pos_y_graphics);
   clReleaseMemObject(buf_satellite_vel_x);
   clReleaseMemObject(buf_satellite_vel_y);

   clReleaseContext(graphics_context);
   clReleaseContext(physics_context);
}







////////////////////////////////////////////////
// ¤¤ TO NOT EDIT ANYTHING AFTER THIS LINE ¤¤ //
////////////////////////////////////////////////

// ¤¤ DO NOT EDIT THIS FUNCTION ¤¤
// Sequential rendering loop used for finding errors
void sequentialGraphicsEngine(){

    // Graphics pixel loop
    for(int i = 0 ;i < SIZE; ++i) {

      // Row wise ordering
      floatvector pixel = {.x = i % WINDOW_WIDTH, .y = i / WINDOW_WIDTH};

      // This color is used for coloring the pixel
      color renderColor = {.red = 0.f, .green = 0.f, .blue = 0.f};

      // Find closest satelite
      float shortestDistance = INFINITY;

      float weights = 0.f;
      int hitsSatellite = 0;

      // First Graphics satelite loop: Find the closest satellite.
      for(int j = 0; j < SATELITE_COUNT; ++j){
         floatvector difference = {.x = pixel.x - satelites[j].position.x,
                                   .y = pixel.y - satelites[j].position.y};
         float distance = sqrt(difference.x * difference.x + 
                               difference.y * difference.y);

         if(distance < SATELITE_RADIUS) {
            renderColor.red = 1.0f;
            renderColor.green = 1.0f;
            renderColor.blue = 1.0f;
            hitsSatellite = 1;
            break;
         } else {
            float weight = 1.0f / (distance*distance*distance*distance);
            weights += weight;
            if(distance < shortestDistance){
               shortestDistance = distance;
               renderColor = satelites[j].identifier;
            }
         }
      }

      // Second graphics loop: Calculate the color based on distance to every satelite.
      if (!hitsSatellite) {
         for(int j = 0; j < SATELITE_COUNT; ++j){
            floatvector difference = {.x = pixel.x - satelites[j].position.x,
                                      .y = pixel.y - satelites[j].position.y};
            float dist2 = (difference.x * difference.x +
                           difference.y * difference.y);
            float weight = 1.0f/(dist2* dist2);

            renderColor.red += (satelites[j].identifier.red *
                                weight /weights) * 3.0f;

            renderColor.green += (satelites[j].identifier.green *
                                  weight / weights) * 3.0f;

            renderColor.blue += (satelites[j].identifier.blue *
                                 weight / weights) * 3.0f;
         }
      }
      correctPixels[i] = renderColor;
    }
}

void sequentialPhysicsEngine(satelite *s){

   // double precision required for accumulation inside this routine,
   // but float storage is ok outside these loops.
   doublevector tmpPosition[SATELITE_COUNT];
   doublevector tmpVelocity[SATELITE_COUNT];

   for (int i = 0; i < SATELITE_COUNT; ++i) {
       tmpPosition[i].x = s[i].position.x;
       tmpPosition[i].y = s[i].position.y;
       tmpVelocity[i].x = s[i].velocity.x;
       tmpVelocity[i].y = s[i].velocity.y;
   }

   // Physics iteration loop
   for(int physicsUpdateIndex = 0;
       physicsUpdateIndex < PHYSICSUPDATESPERFRAME;
      ++physicsUpdateIndex){

       // Physics satelite loop
      for(int i = 0; i < SATELITE_COUNT; ++i){

         // Distance to the blackhole
         // (bit ugly code because C-struct cannot have member functions)
         doublevector positionToBlackHole = {.x = tmpPosition[i].x -
            HORIZONTAL_CENTER, .y = tmpPosition[i].y - VERTICAL_CENTER};
         double distToBlackHoleSquared =
            positionToBlackHole.x * positionToBlackHole.x +
            positionToBlackHole.y * positionToBlackHole.y;
         double distToBlackHole = sqrt(distToBlackHoleSquared);

         // Gravity force
         doublevector normalizedDirection = {
            .x = positionToBlackHole.x / distToBlackHole,
            .y = positionToBlackHole.y / distToBlackHole};
         double accumulation = GRAVITY / distToBlackHoleSquared;

         // Delta time is used to make velocity same despite different FPS
         // Update velocity based on force
         tmpVelocity[i].x -= accumulation * normalizedDirection.x *
            DELTATIME / PHYSICSUPDATESPERFRAME;
         tmpVelocity[i].y -= accumulation * normalizedDirection.y *
            DELTATIME / PHYSICSUPDATESPERFRAME;

         // Update position based on velocity
         tmpPosition[i].x +=
            tmpVelocity[i].x * DELTATIME / PHYSICSUPDATESPERFRAME;
         tmpPosition[i].y +=
            tmpVelocity[i].y * DELTATIME / PHYSICSUPDATESPERFRAME;
      }
   }

   // double precision required for accumulation inside this routine,
   // but float storage is ok outside these loops.
   // copy back the float storage.
   for (int i = 0; i < SATELITE_COUNT; ++i) {
       s[i].position.x = tmpPosition[i].x;
       s[i].position.y = tmpPosition[i].y;
       s[i].velocity.x = tmpVelocity[i].x;
       s[i].velocity.y = tmpVelocity[i].y;
   }
}

// ¤¤ DO NOT EDIT THIS FUNCTION ¤¤
void errorCheck(){
   for(int i=0; i < SIZE; ++i) {
      if(correctPixels[i].red != pixels[i].red ||
         correctPixels[i].green != pixels[i].green ||
         correctPixels[i].blue != pixels[i].blue){ 

         printf("Buggy pixel at (x=%i, y=%i). Press enter to continue.\n", i % WINDOW_WIDTH, i / WINDOW_WIDTH);
         getchar();
         return;
       }
   }
   printf("Error check passed!\n");
}

// ¤¤ DO NOT EDIT THIS FUNCTION ¤¤
void compute(void){
   int timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
   previousFrameTimeSinceStart = timeSinceStart;

   // Error check during first frames
   if (frameNumber < 2) {
      memcpy(backupSatelites, satelites, sizeof(satelite) * SATELITE_COUNT);
      sequentialPhysicsEngine(backupSatelites);
   }
   parallelPhysicsEngine();
   if (frameNumber < 2) {
      for (int i = 0; i < SATELITE_COUNT; i++) {
         if (memcmp (&satelites[i], &backupSatelites[i], sizeof(satelite))) {
            printf("Incorrect satelite data of satelite: %d\n", i);
            getchar();
         }
      }
   }

   int sateliteMovementMoment = glutGet(GLUT_ELAPSED_TIME);
   int sateliteMovementTime = sateliteMovementMoment  - timeSinceStart;

   // Decides the colors for the pixels
   parallelGraphicsEngine();

   int pixelColoringMoment = glutGet(GLUT_ELAPSED_TIME);
   int pixelColoringTime =  pixelColoringMoment - sateliteMovementMoment;

   // Sequential code is used to check possible errors in the parallel version
   if(frameNumber < 2){
      sequentialGraphicsEngine();
      errorCheck();
   }

   int finishTime = glutGet(GLUT_ELAPSED_TIME);
   // Print timings
   int totalTime = finishTime - previousFinishTime;
   previousFinishTime = finishTime;

   printf("Total frametime: %ims, satelite moving: %ims, space coloring: %ims.\n",
      totalTime, sateliteMovementTime, pixelColoringTime);

   // Render the frame
   glutPostRedisplay();
}

// ¤¤ DO NOT EDIT THIS FUNCTION ¤¤
// Probably not the best random number generator
float randomNumber(float min, float max){
   return (rand() * (max - min) / RAND_MAX) + min;
}

// DO NOT EDIT THIS FUNCTION
void fixedInit(unsigned int seed){

   if(seed != 0){
     srand(seed);
   }

   // Init pixel buffer which is rendered to the widow
   pixels = (color*)malloc(sizeof(color) * SIZE);

   // Init pixel buffer which is used for error checking
   correctPixels = (color*)malloc(sizeof(color) * SIZE);

   backupSatelites = (satelite*)malloc(sizeof(satelite) * SATELITE_COUNT);


   // Init satelites buffer which are moving in the space
   satelites = (satelite*)malloc(sizeof(satelite) * SATELITE_COUNT);

   // Create random satelites
   for(int i = 0; i < SATELITE_COUNT; ++i){

      // Random reddish color
      color id = {.red = randomNumber(0.f, 0.15f) + 0.1f,
                  .green = randomNumber(0.f, 0.14f) + 0.0f,
                  .blue = randomNumber(0.f, 0.16f) + 0.0f};
    
      // Random position with margins to borders
      floatvector initialPosition = {.x = HORIZONTAL_CENTER - randomNumber(50, 320),
                              .y = VERTICAL_CENTER - randomNumber(50, 320) };
      initialPosition.x = (i / 2 % 2 == 0) ?
         initialPosition.x : WINDOW_WIDTH - initialPosition.x;
      initialPosition.y = (i < SATELITE_COUNT / 2) ?
         initialPosition.y : WINDOW_HEIGHT - initialPosition.y;

      // Randomize velocity tangential to the balck hole
      floatvector positionToBlackHole = {.x = initialPosition.x - HORIZONTAL_CENTER,
                                    .y = initialPosition.y - VERTICAL_CENTER};
      float distance = (0.06 + randomNumber(-0.01f, 0.01f))/ 
        sqrt(positionToBlackHole.x * positionToBlackHole.x + 
          positionToBlackHole.y * positionToBlackHole.y);
      floatvector initialVelocity = {.x = distance * -positionToBlackHole.y,
                                .y = distance * positionToBlackHole.x};

      // Every other orbits clockwise
      if(i % 2 == 0){
         initialVelocity.x = -initialVelocity.x;
         initialVelocity.y = -initialVelocity.y;
      }

      satelite tmpSatelite = {.identifier = id, .position = initialPosition,
                              .velocity = initialVelocity};
      satelites[i] = tmpSatelite;
   }
}

// ¤¤ DO NOT EDIT THIS FUNCTION ¤¤
void fixedDestroy(void){
   destroy();

   free(pixels);
   free(correctPixels);
   free(satelites);

   if(seed != 0){
     printf("Used seed: %i\n", seed);
   }
}

// ¤¤ DO NOT EDIT THIS FUNCTION ¤¤
// Renders pixels-buffer to the window 
void render(void){
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glDrawPixels(WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGB, GL_FLOAT, pixels);
   glutSwapBuffers();
   frameNumber++;
}

// DO NOT EDIT THIS FUNCTION
// Inits glut and start mainloop
int main(int argc, char** argv){

   if(argc > 1){
     seed = atoi(argv[1]);
     printf("Using seed: %i\n", seed);
   }

   // Init glut window
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
   glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
   glutCreateWindow("Parallelization excercise");
   glutDisplayFunc(render);
   atexit(fixedDestroy);
   previousFrameTimeSinceStart = glutGet(GLUT_ELAPSED_TIME);
   previousFinishTime = glutGet(GLUT_ELAPSED_TIME);
   glEnable(GL_DEPTH_TEST);
   glClearColor(0.0, 0.0, 0.0, 1.0);
   fixedInit(seed);
   init();

   // compute-function is called when everythin from last frame is ready
   glutIdleFunc(compute);

   // Start main loop
   glutMainLoop();
}
