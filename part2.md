# TIE-51257 Parallel computing lab work
## Second part: OpenCL parallelization
- Mika Mäki
- Samaneh Ammari

Please note that this report has been written using
[Markdown](https://en.wikipedia.org/wiki/Markdown), and will look a lot
prettier when viewed with an appropriate editor.

## Program design

Our initial idea was to run the physics on the integrated GPU and graphics on the external GPU.
However, we encountered the difficulty and overhead of transferring data between OpenCL contexts, and decided to compute the physics on the external GPU.
However, this proved out to be much slower than on the CPU, so we decided to put the physics on the CPU.
For this we considered the use of OpenMP inside another threading framework such as std::thread or pthread.
Unfortunately according to the information we found this approach would have been [quite risky](https://stackoverflow.com/questions/24990798/nested-openmp-parallelisation-in-combination-with-stdthread) especially for cross-platform compatibility.
We tested several OpenCL CPU backends and found out that the official Intel CPU backend provided good results.
On Windows this Intel CPU runtime for OpenCL should be included in the integrated graphics driver.
On Linux it should be downloaded separately from the [Intel website](https://software.intel.com/en-us/articles/opencl-drivers).

However, when we were supposed to test our code on the TC219 machines which had OpenCL-capable Intel CPUs, it turned out that they didn't have the OpenCL backend installed.
This was caused by the fact that they didn't have the integrated GPU drivers installed, since their integrated GPUs were disabled.
Presumably this was caused by BIOS settings, and since those settings were password protected, we weren't able to change them.
As a result we had to demonstrate our code on the test machine 1.
However, from the specifications below it can be seen that the hardware we tested on should be less powerful than on the TC219 computers.

It should also be noted that there are slight floating-point differences between the sequentialGraphicsEngine and parallelGraphicsEngine.
A detailed report is available by setting #define DBG_ENABLE to 1.
Also, the (approximately) third frame is displayed twice, since in the first frames the displayed image has to be from the same frame as the satellite positions for the error checking, but the efficient background rendering requires that the image is drawn one frame late.


## Performance results

### Test hardware
Please note that the software on the test machines has been updated since the first part of this project.

#### Test machine 1
- [Intel Core i7-8550U](https://ark.intel.com/content/www/us/en/ark/products/122589/intel-core-i7-8550u-processor-8m-cache-up-to-4-00-ghz.html) (1.8 GHz base clock, ThinkPad T480)
- Kubuntu 19.10
- gcc 9.2.1
- Nvidia driver 435.21 (CUDA 10.1)
- On Windows
    - Windows 10 version 1909
    - MSVC 19.23.28107
    - Nvidia driver 441.22 (CUDA 10.2)
- With external power

#### Test machine 2
- [Intel Core i7-930](https://ark.intel.com/content/www/us/en/ark/products/41447/intel-core-i7-930-processor-8m-cache-2-80-ghz-4-80-gt-s-intel-qpi.html) (overclocked to 3.6 GHz)
- Kubuntu 18.04
- Nvidia driver 440.33.01 (CUDA 10.2)
- gcc 7.4.0

#### Test machine 3
- [Intel Core i9-8950HK](https://ark.intel.com/content/www/us/en/ark/products/134903/intel-core-i9-8950hk-processor-12m-cache-up-to-4-80-ghz.html) (2.9 GHz)
- Windows 10 version 1903
- Visual Studio 2019, MSVC 19.23.28106.4
- Nvidia Quadro P2000


### Results on test machine 1

#### Original code without compiler optimizations
```
Total frametime: 3217ms, satelite moving: 283ms, space coloring: 1445ms.
Total frametime: 1585ms, satelite moving: 130ms, space coloring: 1442ms.
Total frametime: 1591ms, satelite moving: 129ms, space coloring: 1457ms.
Total frametime: 1630ms, satelite moving: 127ms, space coloring: 1497ms.
Total frametime: 1612ms, satelite moving: 130ms, space coloring: 1476ms.
Total frametime: 1625ms, satelite moving: 143ms, space coloring: 1476ms.
Total frametime: 1645ms, satelite moving: 130ms, space coloring: 1510ms.
Total frametime: 1624ms, satelite moving: 131ms, space coloring: 1487ms.
Total frametime: 1647ms, satelite moving: 130ms, space coloring: 1511ms.
Total frametime: 1629ms, satelite moving: 134ms, space coloring: 1490ms.
```

#### Original code with best optimizations
- O2
- march=native
- mtune=native
- ftree-vectorize
- ffast-math

```
Total frametime: 646ms, satelite moving: 60ms, space coloring: 276ms.
Total frametime: 310ms, satelite moving: 29ms, space coloring: 269ms.
Total frametime: 312ms, satelite moving: 28ms, space coloring: 279ms.
Total frametime: 298ms, satelite moving: 26ms, space coloring: 266ms.
Total frametime: 330ms, satelite moving: 27ms, space coloring: 297ms.
Total frametime: 305ms, satelite moving: 27ms, space coloring: 272ms.
Total frametime: 302ms, satelite moving: 28ms, space coloring: 269ms.
Total frametime: 305ms, satelite moving: 28ms, space coloring: 272ms.
Total frametime: 308ms, satelite moving: 30ms, space coloring: 272ms.
Total frametime: 303ms, satelite moving: 27ms, space coloring: 270ms.
```


#### Default WG size (not configured)
The high frame time for the first frame in the OpenCL results is caused by the error message of the floating-point errors being counted in the frame time.

```
Total frametime: 867ms, satelite moving: 29ms, space coloring: 22ms.
Total frametime: 25ms, satelite moving: 1ms, space coloring: 8ms.
Total frametime: 16ms, satelite moving: 2ms, space coloring: 7ms.
Total frametime: 15ms, satelite moving: 2ms, space coloring: 6ms.
Total frametime: 16ms, satelite moving: 2ms, space coloring: 7ms.
Total frametime: 13ms, satelite moving: 1ms, space coloring: 5ms.
Total frametime: 15ms, satelite moving: 1ms, space coloring: 6ms.
Total frametime: 15ms, satelite moving: 1ms, space coloring: 6ms.
Total frametime: 14ms, satelite moving: 1ms, space coloring: 5ms.
Total frametime: 16ms, satelite moving: 0ms, space coloring: 6ms.
```

#### WG size 1x1
With this workgroup size there is significant overhead for the GPU in switching between the workgroups, as it has to do so for each pixel.

```
Total frametime: 1356ms, satelite moving: 31ms, space coloring: 338ms.
Total frametime: 46ms, satelite moving: 1ms, space coloring: 6ms.
Total frametime: 267ms, satelite moving: 253ms, space coloring: 6ms.
Total frametime: 267ms, satelite moving: 256ms, space coloring: 5ms.
Total frametime: 270ms, satelite moving: 258ms, space coloring: 5ms.
Total frametime: 270ms, satelite moving: 257ms, space coloring: 5ms.
Total frametime: 270ms, satelite moving: 258ms, space coloring: 5ms.
Total frametime: 270ms, satelite moving: 258ms, space coloring: 5ms.
Total frametime: 271ms, satelite moving: 258ms, space coloring: 6ms.
Total frametime: 270ms, satelite moving: 257ms, space coloring: 6ms.
```

#### WG size 4x4
```
Total frametime: 1345ms, satelite moving: 28ms, space coloring: 33ms.
Total frametime: 56ms, satelite moving: 1ms, space coloring: 15ms.
Total frametime: 25ms, satelite moving: 12ms, space coloring: 5ms.
Total frametime: 26ms, satelite moving: 13ms, space coloring: 6ms.
Total frametime: 25ms, satelite moving: 13ms, space coloring: 5ms.
Total frametime: 26ms, satelite moving: 12ms, space coloring: 6ms.
Total frametime: 25ms, satelite moving: 13ms, space coloring: 6ms.
Total frametime: 25ms, satelite moving: 12ms, space coloring: 5ms.
Total frametime: 25ms, satelite moving: 13ms, space coloring: 5ms.
Total frametime: 24ms, satelite moving: 9ms, space coloring: 6ms.
```

#### WG size 8x4
It appears that this worksize reaches the optimum, and further increases in the worksize have no effect.

```
Total frametime: 889ms, satelite moving: 29ms, space coloring: 23ms.
Total frametime: 41ms, satelite moving: 1ms, space coloring: 10ms.
Total frametime: 15ms, satelite moving: 2ms, space coloring: 6ms.
Total frametime: 15ms, satelite moving: 1ms, space coloring: 6ms.
Total frametime: 15ms, satelite moving: 0ms, space coloring: 6ms.
Total frametime: 15ms, satelite moving: 0ms, space coloring: 6ms.
Total frametime: 15ms, satelite moving: 3ms, space coloring: 5ms.
Total frametime: 16ms, satelite moving: 2ms, space coloring: 7ms.
Total frametime: 15ms, satelite moving: 1ms, space coloring: 6ms.
Total frametime: 16ms, satelite moving: 2ms, space coloring: 7ms.
Total frametime: 17ms, satelite moving: 0ms, space coloring: 7ms.
```

#### WG size 8x8
```
Total frametime: 1083ms, satelite moving: 30ms, space coloring: 31ms.
Total frametime: 56ms, satelite moving: 8ms, space coloring: 6ms.
Total frametime: 26ms, satelite moving: 12ms, space coloring: 7ms.
Total frametime: 16ms, satelite moving: 2ms, space coloring: 6ms.
Total frametime: 18ms, satelite moving: 1ms, space coloring: 7ms.
Total frametime: 15ms, satelite moving: 0ms, space coloring: 6ms.
Total frametime: 14ms, satelite moving: 1ms, space coloring: 5ms.
Total frametime: 15ms, satelite moving: 1ms, space coloring: 5ms.
Total frametime: 15ms, satelite moving: 1ms, space coloring: 5ms.
Total frametime: 15ms, satelite moving: 3ms, space coloring: 5ms.
```

#### WG size 16x16
```
Total frametime: 791ms, satelite moving: 29ms, space coloring: 22ms.
Total frametime: 32ms, satelite moving: 1ms, space coloring: 10ms.
Total frametime: 15ms, satelite moving: 0ms, space coloring: 6ms.
Total frametime: 15ms, satelite moving: 1ms, space coloring: 6ms.
Total frametime: 15ms, satelite moving: 1ms, space coloring: 6ms.
Total frametime: 15ms, satelite moving: 1ms, space coloring: 6ms.
Total frametime: 14ms, satelite moving: 1ms, space coloring: 5ms.
Total frametime: 15ms, satelite moving: 1ms, space coloring: 6ms.
Total frametime: 15ms, satelite moving: 0ms, space coloring: 6ms.
Total frametime: 15ms, satelite moving: 2ms, space coloring: 6ms.
```

#### WG size 32x32
```
Total frametime: 1272ms, satelite moving: 30ms, space coloring: 32ms.
Total frametime: 31ms, satelite moving: 1ms, space coloring: 15ms.
Total frametime: 18ms, satelite moving: 4ms, space coloring: 8ms.
Total frametime: 15ms, satelite moving: 2ms, space coloring: 6ms.
Total frametime: 16ms, satelite moving: 0ms, space coloring: 7ms.
Total frametime: 16ms, satelite moving: 2ms, space coloring: 7ms.
Total frametime: 16ms, satelite moving: 3ms, space coloring: 7ms.
Total frametime: 13ms, satelite moving: 1ms, space coloring: 5ms.
Total frametime: 15ms, satelite moving: 1ms, space coloring: 5ms.
Total frametime: 15ms, satelite moving: 1ms, space coloring: 5ms.
```

#### WG size 64x64
This workgroup size results randomly in either a segfault or a black screen.
According to clGetDeviceInfo() the maximum workgroup size for Nvidia MX150 is 1024 = 32x32, so it's not surprising that workgroup sizes larger than this don't work.
This limitation arises from the hardware capabilities of a single compute unit (or whatever it's called depending on the vendor), since a compute unit has to be able to maintain state for all elements in the workgroup simulateneously and switch between them, so that barriers and other similar mechanimsms work properly.

```
Segmentation fault (core dumped)
```

#### Window size 80x80, original code with optimizations
We presume that "with optimizations on" should be interpreted as "with best optimizations" as in the case 2 of the previous questions, and since in our previous tests the -O3 flag has produced worse performance than -O2, and neither of these includes the important vectorization. Therefore this test has been run with the best optimizations (march=native etc.) described above.

```
Total frametime: 74ms, satelite moving: 61ms, space coloring: 2ms.
Total frametime: 33ms, satelite moving: 29ms, space coloring: 2ms.
Total frametime: 30ms, satelite moving: 29ms, space coloring: 1ms.
Total frametime: 31ms, satelite moving: 29ms, space coloring: 2ms.
Total frametime: 30ms, satelite moving: 28ms, space coloring: 2ms.
Total frametime: 31ms, satelite moving: 28ms, space coloring: 2ms.
Total frametime: 29ms, satelite moving: 27ms, space coloring: 2ms.
Total frametime: 30ms, satelite moving: 29ms, space coloring: 1ms.
Total frametime: 31ms, satelite moving: 29ms, space coloring: 1ms.
Total frametime: 32ms, satelite moving: 29ms, space coloring: 2ms.
```

#### Window size 80x80, OpenCL, default workgroup size
```
Total frametime: 2405ms, satelite moving: 33ms, space coloring: 1ms.
Total frametime: 12ms, satelite moving: 1ms, space coloring: 3ms.
Total frametime: 12ms, satelite moving: 10ms, space coloring: 1ms.
Total frametime: 11ms, satelite moving: 10ms, space coloring: 0ms.
Total frametime: 11ms, satelite moving: 10ms, space coloring: 0ms.
Total frametime: 11ms, satelite moving: 9ms, space coloring: 1ms.
Total frametime: 10ms, satelite moving: 4ms, space coloring: 0ms.
Total frametime: 13ms, satelite moving: 0ms, space coloring: 0ms.
Total frametime: 17ms, satelite moving: 0ms, space coloring: 1ms.
Total frametime: 16ms, satelite moving: 0ms, space coloring: 0ms.
```

#### Window size 80x80, OpenCL, worgroup size 1x1
The results with other workgroup sizes did not differ much.
This is presumably caused by the fact that most of the time is spent in the control flow and OpenGL drawing, which happen outside the engine functions we have written.
There is no need to wait for either kernel, since they are executed in the background faster than the control flow and drawing of the main program, as the last lines of the frametime output demonstrate.
As a result, time is spent mostly in transferring data to the OpenCL backends, and this time is independent of the workgroup size.
If the kernel execution would not be done in the background, the overall frametime could have been longer on the GPU, since there is some overhead and latency with each transfer to and from the GPU.

```
Total frametime: 1301ms, satelite moving: 36ms, space coloring: 2ms.
Total frametime: 6ms, satelite moving: 0ms, space coloring: 1ms.
Total frametime: 9ms, satelite moving: 9ms, space coloring: 0ms.
Total frametime: 10ms, satelite moving: 10ms, space coloring: 0ms.
Total frametime: 9ms, satelite moving: 9ms, space coloring: 0ms.
Total frametime: 10ms, satelite moving: 9ms, space coloring: 0ms.
Total frametime: 12ms, satelite moving: 9ms, space coloring: 0ms.
Total frametime: 11ms, satelite moving: 4ms, space coloring: 0ms.
Total frametime: 13ms, satelite moving: 0ms, space coloring: 0ms.
Total frametime: 17ms, satelite moving: 0ms, space coloring: 0ms.
```

### Results on test machine 1 with Windows and MSVC

#### Original code with best compiler optimizations
- O2
- arch:AVX2
- fp:fast

```
Total frametime: 827ms, satelite moving: 217ms, space coloring: 289ms.
Total frametime: 398ms, satelite moving: 107ms, space coloring: 287ms.
Total frametime: 404ms, satelite moving: 109ms, space coloring: 293ms.
Total frametime: 402ms, satelite moving: 110ms, space coloring: 289ms.
Total frametime: 400ms, satelite moving: 110ms, space coloring: 288ms.
Total frametime: 403ms, satelite moving: 110ms, space coloring: 290ms.
Total frametime: 398ms, satelite moving: 109ms, space coloring: 287ms.
Total frametime: 401ms, satelite moving: 109ms, space coloring: 290ms.
Total frametime: 398ms, satelite moving: 108ms, space coloring: 288ms.
Total frametime: 399ms, satelite moving: 109ms, space coloring: 288ms.
```

#### Original code without fp:fast
For comparing with test machine 3
- O2
- arch:AVX2

```
Total frametime: 1683ms, satelite moving: 354ms, space coloring: 640ms.
Total frametime: 819ms, satelite moving: 174ms, space coloring: 637ms.
Total frametime: 817ms, satelite moving: 178ms, space coloring: 635ms.
Total frametime: 820ms, satelite moving: 174ms, space coloring: 644ms.
Total frametime: 819ms, satelite moving: 175ms, space coloring: 641ms.
Total frametime: 816ms, satelite moving: 175ms, space coloring: 639ms.
Total frametime: 816ms, satelite moving: 175ms, space coloring: 639ms.
Total frametime: 818ms, satelite moving: 174ms, space coloring: 641ms.
Total frametime: 816ms, satelite moving: 176ms, space coloring: 638ms.
Total frametime: 814ms, satelite moving: 173ms, space coloring: 638ms.
```

#### Default WG size (not configured)
```
Total frametime: 2350ms, satelite moving: 172ms, space coloring: 19ms.
Total frametime: 36ms, satelite moving: 12ms, space coloring: 6ms.
Total frametime: 27ms, satelite moving: 11ms, space coloring: 7ms.
Total frametime: 26ms, satelite moving: 12ms, space coloring: 6ms.
Total frametime: 28ms, satelite moving: 11ms, space coloring: 7ms.
Total frametime: 28ms, satelite moving: 11ms, space coloring: 8ms.
Total frametime: 25ms, satelite moving: 12ms, space coloring: 7ms.
Total frametime: 26ms, satelite moving: 11ms, space coloring: 8ms.
Total frametime: 24ms, satelite moving: 12ms, space coloring: 6ms.
Total frametime: 26ms, satelite moving: 12ms, space coloring: 7ms.
```

### Results on test machine 1 with cross-compiled binaries
The compilation used march=native, mtune=native on the GitLab CI/CD server, so it's not optimal, but the original code runs faster than the version compiled locally with MSVC.

#### Original code with best compiler optimizations
```
Total frametime: 732ms, satelite moving: 32ms, space coloring: 335ms.
Total frametime: 365ms, satelite moving: 16ms, space coloring: 346ms.
Total frametime: 349ms, satelite moving: 15ms, space coloring: 332ms.
Total frametime: 349ms, satelite moving: 16ms, space coloring: 331ms.
Total frametime: 352ms, satelite moving: 16ms, space coloring: 334ms.
Total frametime: 351ms, satelite moving: 16ms, space coloring: 333ms.
Total frametime: 350ms, satelite moving: 15ms, space coloring: 333ms.
Total frametime: 350ms, satelite moving: 15ms, space coloring: 332ms.
Total frametime: 350ms, satelite moving: 15ms, space coloring: 333ms.
Total frametime: 346ms, satelite moving: 16ms, space coloring: 328ms.
```

#### Default WG size (not configured)
With OpenCL there is no significant difference compared to the MSVC version, since the heavy computations are done by the OpenCL backends.
However, with both compilers the frame times are significantly longer on Windows.
This could be caused by for example the Linux CPU governor allowing a more aggressive use of higher frequencies, although in general the laptop is much more quiet on Linux, which would imply the opposite.
It could also be that with Windows there are more background processes, and this results in more competition for the CPU resources and context-switching.

```
Total frametime: 885ms, satelite moving: 21ms, space coloring: 22ms.
Total frametime: 29ms, satelite moving: 12ms, space coloring: 7ms.
Total frametime: 24ms, satelite moving: 12ms, space coloring: 6ms.
Total frametime: 26ms, satelite moving: 11ms, space coloring: 8ms.
Total frametime: 25ms, satelite moving: 11ms, space coloring: 6ms.
Total frametime: 26ms, satelite moving: 12ms, space coloring: 7ms.
Total frametime: 23ms, satelite moving: 11ms, space coloring: 6ms.
Total frametime: 26ms, satelite moving: 11ms, space coloring: 7ms.
Total frametime: 25ms, satelite moving: 11ms, space coloring: 7ms.
Total frametime: 27ms, satelite moving: 11ms, space coloring: 7ms.
```

### Results on test machine 2

#### Original code without compiler optimizations
```
Total frametime: 5765ms, satelite moving: 666ms, space coloring: 2544ms.
Total frametime: 2855ms, satelite moving: 331ms, space coloring: 2510ms.
Total frametime: 2878ms, satelite moving: 331ms, space coloring: 2540ms.
Total frametime: 2855ms, satelite moving: 336ms, space coloring: 2511ms.
Total frametime: 2853ms, satelite moving: 331ms, space coloring: 2514ms.
Total frametime: 2847ms, satelite moving: 331ms, space coloring: 2508ms.
Total frametime: 2847ms, satelite moving: 333ms, space coloring: 2507ms.
Total frametime: 2851ms, satelite moving: 332ms, space coloring: 2512ms.
Total frametime: 2851ms, satelite moving: 331ms, space coloring: 2512ms.
Total frametime: 2848ms, satelite moving: 331ms, space coloring: 2509ms.
```

#### Original code with best compiler optimizations
```
Total frametime: 2104ms, satelite moving: 179ms, space coloring: 951ms.
Total frametime: 1049ms, satelite moving: 90ms, space coloring: 946ms.
Total frametime: 1044ms, satelite moving: 91ms, space coloring: 946ms.
Total frametime: 1040ms, satelite moving: 89ms, space coloring: 944ms.
Total frametime: 1039ms, satelite moving: 89ms, space coloring: 942ms.
Total frametime: 1040ms, satelite moving: 89ms, space coloring: 943ms.
Total frametime: 1040ms, satelite moving: 89ms, space coloring: 944ms.
Total frametime: 1039ms, satelite moving: 89ms, space coloring: 943ms.
Total frametime: 1041ms, satelite moving: 89ms, space coloring: 944ms.
Total frametime: 1040ms, satelite moving: 89ms, space coloring: 944ms.
```

#### Default WG size (not configured)
```
Total frametime: 1777ms, satelite moving: 108ms, space coloring: 24ms.
Total frametime: 26ms, satelite moving: 6ms, space coloring: 4ms.
Total frametime: 39ms, satelite moving: 24ms, space coloring: 4ms.
Total frametime: 42ms, satelite moving: 24ms, space coloring: 4ms.
Total frametime: 42ms, satelite moving: 21ms, space coloring: 4ms.
Total frametime: 42ms, satelite moving: 22ms, space coloring: 4ms.
Total frametime: 41ms, satelite moving: 23ms, space coloring: 4ms.
Total frametime: 43ms, satelite moving: 22ms, space coloring: 4ms.
Total frametime: 43ms, satelite moving: 22ms, space coloring: 5ms.
Total frametime: 42ms, satelite moving: 20ms, space coloring: 4ms.
```

### Results on test machine 3

#### Original code without compiler optimizations
```
Total frametime: 4076ms, satelite moving: 367ms, space coloring: 1797ms.
Total frametime: 1971ms, satelite moving: 160ms, space coloring: 1797ms.
Total frametime: 2007ms, satelite moving: 158ms, space coloring: 1843ms.
Total frametime: 1961ms, satelite moving: 156ms, space coloring: 1799ms.
Total frametime: 1967ms, satelite moving: 163ms, space coloring: 1799ms.
Total frametime: 1968ms, satelite moving: 159ms, space coloring: 1804ms.
Total frametime: 1966ms, satelite moving: 158ms, space coloring: 1802ms.
Total frametime: 2006ms, satelite moving: 197ms, space coloring: 1803ms.
Total frametime: 2007ms, satelite moving: 197ms, space coloring: 1804ms.
Total frametime: 1974ms, satelite moving: 159ms, space coloring: 1804ms.
```

#### Original code with best optimizations
- O2
- arch:AVX2
- (note that fp:fast was not used here since it was missing from our build script at that time)

```
Total frametime: 2299ms, satelite moving: 501ms, space coloring: 839ms.
Total frametime: 1073ms, satelite moving: 229ms, space coloring: 835ms.
Total frametime: 1068ms, satelite moving: 227ms, space coloring: 838ms.
Total frametime: 1071ms, satelite moving: 227ms, space coloring: 841ms.
Total frametime: 1073ms, satelite moving: 228ms, space coloring: 843ms.
Total frametime: 1123ms, satelite moving: 278ms, space coloring: 841ms.
Total frametime: 1069ms, satelite moving: 227ms, space coloring: 839ms.
Total frametime: 1073ms, satelite moving: 227ms, space coloring: 843ms.
Total frametime: 1116ms, satelite moving: 266ms, space coloring: 847ms.
Total frametime: 1117ms, satelite moving: 275ms, space coloring: 839ms.
```


#### Default WG size (not configured)
```
Total frametime: 52ms, satelite moving: 7ms, space coloring: 5ms.
Total frametime: 39ms, satelite moving: 8ms, space coloring: 10ms.
Total frametime: 51ms, satelite moving: 8ms, space coloring: 11ms.
Total frametime: 50ms, satelite moving: 7ms, space coloring: 11ms.
Total frametime: 49ms, satelite moving: 8ms, space coloring: 10ms.
Total frametime: 50ms, satelite moving: 8ms, space coloring: 10ms.
Total frametime: 50ms, satelite moving: 8ms, space coloring: 11ms.
Total frametime: 49ms, satelite moving: 8ms, space coloring: 10ms.
Total frametime: 50ms, satelite moving: 7ms, space coloring: 10ms.
Total frametime: 50ms, satelite moving: 7ms, space coloring: 11ms.
```

#### WG size 1x1
```
Total frametime: 5688ms, satelite moving: 256ms, space coloring: 208ms.
Total frametime: 229ms, satelite moving: 183ms, space coloring: 7ms.
Total frametime: 198ms, satelite moving: 164ms, space coloring: 7ms.
Total frametime: 194ms, satelite moving: 158ms, space coloring: 7ms.
Total frametime: 184ms, satelite moving: 159ms, space coloring: 7ms.
Total frametime: 183ms, satelite moving: 158ms, space coloring: 7ms.
Total frametime: 183ms, satelite moving: 159ms, space coloring: 7ms.
Total frametime: 184ms, satelite moving: 160ms, space coloring: 7ms.
Total frametime: 184ms, satelite moving: 159ms, space coloring: 6ms.
Total frametime: 183ms, satelite moving: 159ms, space coloring: 6ms.
```

#### WG size 2x2
```
Total frametime: 4340ms, satelite moving: 257ms, space coloring: 68ms.
Total frametime: 91ms, satelite moving: 52ms, space coloring: 7ms.
Total frametime: 85ms, satelite moving: 51ms, space coloring: 9ms.
Total frametime: 80ms, satelite moving: 49ms, space coloring: 8ms.
Total frametime: 80ms, satelite moving: 47ms, space coloring: 7ms.
Total frametime: 83ms, satelite moving: 45ms, space coloring: 9ms.
Total frametime: 79ms, satelite moving: 42ms, space coloring: 7ms.
Total frametime: 67ms, satelite moving: 42ms, space coloring: 7ms.
Total frametime: 65ms, satelite moving: 41ms, space coloring: 7ms.
Total frametime: 65ms, satelite moving: 40ms, space coloring: 6ms.
```

#### WG size 4x4
```
Total frametime: 128797ms, satelite moving: 246ms, space coloring: 24ms.
Total frametime: 57ms, satelite moving: 13ms, space coloring: 5ms.
Total frametime: 34ms, satelite moving: 14ms, space coloring: 5ms.
Total frametime: 52ms, satelite moving: 13ms, space coloring: 8ms.
Total frametime: 50ms, satelite moving: 13ms, space coloring: 8ms.
Total frametime: 51ms, satelite moving: 14ms, space coloring: 8ms.
Total frametime: 50ms, satelite moving: 13ms, space coloring: 8ms.
Total frametime: 58ms, satelite moving: 13ms, space coloring: 16ms.
Total frametime: 38ms, satelite moving: 13ms, space coloring: 4ms.
Total frametime: 33ms, satelite moving: 13ms, space coloring: 4ms.
```

#### WG size 8x4

```
Total frametime: 2984ms, satelite moving: 235ms, space coloring: 17ms.
Total frametime: 32ms, satelite moving: 7ms, space coloring: 6ms.
Total frametime: 40ms, satelite moving: 7ms, space coloring: 12ms.
Total frametime: 50ms, satelite moving: 7ms, space coloring: 12ms.
Total frametime: 49ms, satelite moving: 8ms, space coloring: 11ms.
Total frametime: 49ms, satelite moving: 6ms, space coloring: 11ms.
Total frametime: 52ms, satelite moving: 7ms, space coloring: 12ms.
Total frametime: 49ms, satelite moving: 7ms, space coloring: 11ms.
Total frametime: 51ms, satelite moving: 7ms, space coloring: 12ms.
Total frametime: 49ms, satelite moving: 7ms, space coloring: 11ms.
```

#### WG size 8x8
```
Total frametime: 4824ms, satelite moving: 257ms, space coloring: 20ms.
Total frametime: 35ms, satelite moving: 7ms, space coloring: 5ms.
Total frametime: 40ms, satelite moving: 8ms, space coloring: 12ms.
Total frametime: 48ms, satelite moving: 8ms, space coloring: 10ms.
Total frametime: 51ms, satelite moving: 8ms, space coloring: 11ms.
Total frametime: 49ms, satelite moving: 7ms, space coloring: 11ms.
Total frametime: 52ms, satelite moving: 8ms, space coloring: 12ms.
Total frametime: 49ms, satelite moving: 7ms, space coloring: 11ms.
Total frametime: 50ms, satelite moving: 7ms, space coloring: 11ms.
Total frametime: 50ms, satelite moving: 7ms, space coloring: 11ms.
```

#### WG size 16x16
```
Total frametime: 15667ms, satelite moving: 280ms, space coloring: 17ms.
Total frametime: 52ms, satelite moving: 7ms, space coloring: 5ms.
Total frametime: 40ms, satelite moving: 7ms, space coloring: 11ms.
Total frametime: 49ms, satelite moving: 8ms, space coloring: 10ms.
Total frametime: 50ms, satelite moving: 7ms, space coloring: 11ms.
Total frametime: 50ms, satelite moving: 7ms, space coloring: 11ms.
Total frametime: 51ms, satelite moving: 7ms, space coloring: 12ms.
Total frametime: 49ms, satelite moving: 7ms, space coloring: 11ms.
Total frametime: 50ms, satelite moving: 7ms, space coloring: 11ms.
Total frametime: 49ms, satelite moving: 7ms, space coloring: 10ms.
```

#### WG size 32x32
```
Total frametime: 3472ms, satelite moving: 309ms, space coloring: 18ms.
Total frametime: 30ms, satelite moving: 7ms, space coloring: 6ms.
Total frametime: 39ms, satelite moving: 8ms, space coloring: 10ms.
Total frametime: 50ms, satelite moving: 7ms, space coloring: 11ms.
Total frametime: 50ms, satelite moving: 8ms, space coloring: 11ms.
Total frametime: 49ms, satelite moving: 7ms, space coloring: 11ms.
Total frametime: 50ms, satelite moving: 7ms, space coloring: 11ms.
Total frametime: 50ms, satelite moving: 7ms, space coloring: 11ms.
Total frametime: 50ms, satelite moving: 8ms, space coloring: 10ms.
Total frametime: 49ms, satelite moving: 6ms, space coloring: 10ms.
Total frametime: 52ms, satelite moving: 7ms, space coloring: 12ms.
```

#### WG size 64x64
This workgroup size results a black screen.

```
Segmentation fault (core dumped)
```

#### Window size 80x80, original code with optimizations

```
Total frametime: 5814ms, satelite moving: 260ms, space coloring: 2ms.
Total frametime: 25ms, satelite moving: 2ms, space coloring: 0ms.
Total frametime: 17ms, satelite moving: 2ms, space coloring: 1ms.
Total frametime: 16ms, satelite moving: 2ms, space coloring: 0ms.
Total frametime: 17ms, satelite moving: 2ms, space coloring: 0ms.
Total frametime: 17ms, satelite moving: 3ms, space coloring: 0ms.
Total frametime: 17ms, satelite moving: 3ms, space coloring: 0ms.
Total frametime: 17ms, satelite moving: 2ms, space coloring: 1ms.
Total frametime: 16ms, satelite moving: 3ms, space coloring: 0ms.
Total frametime: 16ms, satelite moving: 2ms, space coloring: 0ms.
```

#### Window size 80x80, OpenCL, default workgroup size
```
Total frametime: 7707ms, satelite moving: 254ms, space coloring: 0ms.
Total frametime: 41ms, satelite moving: 1ms, space coloring: 0ms.
Total frametime: 17ms, satelite moving: 1ms, space coloring: 0ms.
Total frametime: 30ms, satelite moving: 14ms, space coloring: 1ms.
Total frametime: 32ms, satelite moving: 13ms, space coloring: 1ms.
Total frametime: 32ms, satelite moving: 11ms, space coloring: 1ms.
Total frametime: 22ms, satelite moving: 1ms, space coloring: 0ms.
Total frametime: 19ms, satelite moving: 3ms, space coloring: 0ms.
Total frametime: 14ms, satelite moving: 1ms, space coloring: 0ms.
Total frametime: 17ms, satelite moving: 1ms, space coloring: 0ms.
```

#### Window size 80x80, OpenCL, workgroup size 1x1
The results with other workgroup sizes did not differ much.

```
Total frametime: 4855ms, satelite moving: 270ms, space coloring: 1ms.
Total frametime: 22ms, satelite moving: 1ms, space coloring: 1ms.
Total frametime: 16ms, satelite moving: 2ms, space coloring: 0ms.
Total frametime: 19ms, satelite moving: 4ms, space coloring: 0ms.
Total frametime: 31ms, satelite moving: 19ms, space coloring: 0ms.
Total frametime: 32ms, satelite moving: 17ms, space coloring: 0ms.
Total frametime: 30ms, satelite moving: 13ms, space coloring: 1ms.
Total frametime: 22ms, satelite moving: 3ms, space coloring: 0ms.
Total frametime: 22ms, satelite moving: 8ms, space coloring: 0ms.
Total frametime: 23ms, satelite moving: 13ms, space coloring: 1ms.
```


# Feedback

#### 0. What was the most difficult thing in this exercise work?

C and OpenCL are so low-level languages that there is a lot of boilerplate code, and even the smallest mistakes can cause segmentation faults that are difficult to debug.
There were several cases when some array initialization appeared correct but caused mysterious segmentation faults in entirely different parts of the program.
OpenCL segfaults were also difficult to debug, as gdb did not provide proper information on where and when they happened, even though the program was compiled with the -g and -ggdb flags.
As a result, in the future when we are going to write more OpenCL code we are looking forward to using more memory-safe languages such as [Rust](https://github.com/cogciprocate/ocl) and [Python](https://github.com/inducer/pyopencl).

#### 1. What was good in this exercise work?

The exercise work gave us a hands-on training on the implementation of highly parallel programs, and it gave us a good challenge for testing the limits of our understanding.
We also had quite a bit of fun in the sense of [funroll-loops](https://forums.gentoo.org/viewtopic-t-309752-start-0.html). [Undefined behaviour is magic!](http://blog.llvm.org/2016/04/undefined-behavior-is-magic.html)

#### 2. How would you improve this exercise work?

There appears to be a bug on the line "#define SIZE WINDOW_HEIGHT*WINDOW_HEIGHT".
Shouldn't it be "#define SIZE WINDOW_WIDTH*WINDOW_HEIGHT"?

There could be more compilation examples, and it would be great to have something like (or even exact copies of) the Docker, CI/CD and MSVC scripts we developed in the future implementations of this course.
Especially the MSVC scripts proved out to be useful, since specifying all the necessary library linking using the Visual Studio GUI proved out to be unnecessarily complicated.

There could also be template code for loading OpenCL code from separate files, as this greatly simplifies editing the code with an IDE where several files can be opened side-by-side.
However, there were some tricks such as using the mode "rb" in fopen() to avoid problems with the CRLF line endings on Windows.
With the default mode "r" the OpenCL backend gave weird syntax errors and sometimes notifications that the input was not valid UTF-8.

It should not be necessary to sign an NDA to get access to computers that are necessary for this course.
Please see the Free Software Foundation article [Why Schools Should Exclusively Use Free Software](https://www.gnu.org/education/edu-schools.en.html) for the moral problems of this arrangement.

#### What was the most important and/or interesting thing you learned from this exercise work?

OpenCL!
It's going to be highly useful in our work, and we are very thankful that this course taught us an open and cross-platform language, and not a vendor-specific one.
Now that we have a fully functional example of the OpenCL boilerplate and we understand how it works, it should be doable to use it for some actually useful physics simulations.

#### Approximately how many hours did it take to complete this exercise work

A lot, at least several full workdays.
In the first part a significant factor of the total time was spent on trying different general optimizations for the code, and most of these ended up having a negative effect on the overall performance.
On the second part a significant amount of time went into debugging and writing boilerplate code.
However, the time spent on this exercise work was entirely compensated by the weekly homework, which was really quick to do compared to most other courses.


<!--
# Extra

On test machine 1 installing the [BIOS update](https://download.lenovo.com/pccbbs/mobiles/n24ul15w.txt) that fixes the Intel [ZombieLoad v2](https://zombieloadattack.com/) vulnerabilities increased the total frame times by a bit over 20 ms for the original code with best compiler optimizations.
This is a bit surprising, since [the CPU of that machine](https://ark.intel.com/content/www/us/en/ark/products/122589/intel-core-i7-8550u-processor-8m-cache-up-to-4-00-ghz.html) does not even have the problematic TSX-NI instructions.
Test machine 2 is too old to receive BIOS updates, and we haven't had the time to modify the BIOS ourselves.

```
Total frametime: 650ms, satelite moving: 34ms, space coloring: 282ms.
Total frametime: 314ms, satelite moving: 17ms, space coloring: 291ms.
Total frametime: 316ms, satelite moving: 19ms, space coloring: 291ms.
Total frametime: 300ms, satelite moving: 18ms, space coloring: 278ms.
Total frametime: 305ms, satelite moving: 18ms, space coloring: 282ms.
Total frametime: 312ms, satelite moving: 19ms, space coloring: 287ms.
Total frametime: 302ms, satelite moving: 18ms, space coloring: 280ms.
Total frametime: 302ms, satelite moving: 18ms, space coloring: 280ms.
Total frametime: 308ms, satelite moving: 18ms, space coloring: 285ms.
Total frametime: 318ms, satelite moving: 18ms, space coloring: 295ms.
```

Also upgrading the kernel to the latest available version (5.0.0-36) improves the frame times a bit, but there is still a hit of about 5-10 ms.
It could also be that the CPU was running a bit warmer in the previous measurements, although there are no particular reasons why this would be the case.

```
Total frametime: 617ms, satelite moving: 35ms, space coloring: 265ms.
Total frametime: 291ms, satelite moving: 17ms, space coloring: 268ms.
Total frametime: 282ms, satelite moving: 17ms, space coloring: 260ms.
Total frametime: 296ms, satelite moving: 17ms, space coloring: 275ms.
Total frametime: 285ms, satelite moving: 17ms, space coloring: 264ms.
Total frametime: 286ms, satelite moving: 17ms, space coloring: 265ms.
Total frametime: 284ms, satelite moving: 17ms, space coloring: 263ms.
Total frametime: 291ms, satelite moving: 16ms, space coloring: 270ms.
Total frametime: 288ms, satelite moving: 17ms, space coloring: 267ms.
Total frametime: 286ms, satelite moving: 17ms, space coloring: 265ms.
```
-->
