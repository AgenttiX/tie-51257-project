# TIE-51257 Parallel computing lab work
## First part: CPU parallelization
- Mika Mäki
- Samaneh Ammari

Please note that this report has been written using [Markdown](https://en.wikipedia.org/wiki/Markdown), and will look a lot prettier when viewed with an appropriate editor.

## 6.1 Benchmarking

#### Test machine 1
- Intel Core i7-8550U (1.8 GHz base clock, ThinkPad T480)
- Kubuntu 19.04
- gcc 8.3.0
- With external power

#### Test machine 2
- Intel Core i7-930 (overclocked to 3.6 GHz)
- Kubuntu 18.04
- gcc 7.4.0

#### Test machine 3
- Intel Core i9-8950HK (2.9 GHz)
- Windows 10 version 1903
- Visual Studio 2019, MSVC 19.23.28106.4

### 1. Original code without any optimization flags
`gcc -o ./build/parallel_no-opt ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall`

#### Test machine 1
```
Total frametime: 3049ms, satelite moving: 247ms, space coloring: 1374ms.
Total frametime: 1509ms, satelite moving: 117ms, space coloring: 1380ms.
Total frametime: 1477ms, satelite moving: 118ms, space coloring: 1353ms.
Total frametime: 1481ms, satelite moving: 122ms, space coloring: 1354ms.
Total frametime: 1472ms, satelite moving: 119ms, space coloring: 1348ms.
Total frametime: 1480ms, satelite moving: 116ms, space coloring: 1358ms.
Total frametime: 1483ms, satelite moving: 119ms, space coloring: 1359ms.
Total frametime: 1499ms, satelite moving: 117ms, space coloring: 1376ms.
Total frametime: 1497ms, satelite moving: 119ms, space coloring: 1372ms.
Total frametime: 1498ms, satelite moving: 119ms, space coloring: 1374ms.
```

#### Test machine 2
```
Total frametime: 5958ms, satelite moving: 681ms, space coloring: 2584ms.
Total frametime: 2934ms, satelite moving: 340ms, space coloring: 2582ms.
Total frametime: 2884ms, satelite moving: 342ms, space coloring: 2535ms.
Total frametime: 2865ms, satelite moving: 332ms, space coloring: 2525ms.
Total frametime: 2872ms, satelite moving: 335ms, space coloring: 2530ms.
Total frametime: 2967ms, satelite moving: 332ms, space coloring: 2628ms.
Total frametime: 2921ms, satelite moving: 340ms, space coloring: 2573ms.
Total frametime: 2930ms, satelite moving: 339ms, space coloring: 2583ms.
Total frametime: 2928ms, satelite moving: 340ms, space coloring: 2581ms.
Total frametime: 2932ms, satelite moving: 339ms, space coloring: 2585ms.
```

#### Test machine 3
```
Total frametime: 3180ms, satelite moving: 248ms, space coloring: 1511ms.
Total frametime: 1484ms, satelite moving: 114ms, space coloring: 1365ms.
Total frametime: 1511ms, satelite moving: 117ms, space coloring: 1392ms.
Total frametime: 1535ms, satelite moving: 125ms, space coloring: 1395ms.
Total frametime: 1506ms, satelite moving: 115ms, space coloring: 1391ms.
Total frametime: 1517ms, satelite moving: 118ms, space coloring: 1399ms.
Total frametime: 1518ms, satelite moving: 116ms, space coloring: 1402ms.
Total frametime: 1537ms, satelite moving: 117ms, space coloring: 1420ms.
Total frametime: 1533ms, satelite moving: 115ms, space coloring: 1417ms.
Total frametime: 1531ms, satelite moving: 116ms, space coloring: 1414ms.
```

### 2. Original code with most compiler optimizations (-O2)
`gcc -o ./build/parallel_O2 ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2`

#### Test machine 1
```
Total frametime: 1119ms, satelite moving: 134ms, space coloring: 476ms.
Total frametime: 540ms, satelite moving: 61ms, space coloring: 466ms.
Total frametime: 539ms, satelite moving: 61ms, space coloring: 472ms.
Total frametime: 543ms, satelite moving: 61ms, space coloring: 477ms.
Total frametime: 545ms, satelite moving: 61ms, space coloring: 479ms.
Total frametime: 533ms, satelite moving: 61ms, space coloring: 466ms.
Total frametime: 540ms, satelite moving: 62ms, space coloring: 473ms.
Total frametime: 534ms, satelite moving: 61ms, space coloring: 467ms.
Total frametime: 538ms, satelite moving: 61ms, space coloring: 472ms.
Total frametime: 534ms, satelite moving: 61ms, space coloring: 467ms.
```

#### Test machine 2
```
Total frametime: 4220ms, satelite moving: 667ms, space coloring: 1731ms.
Total frametime: 2070ms, satelite moving: 333ms, space coloring: 1726ms.
Total frametime: 2068ms, satelite moving: 334ms, space coloring: 1727ms.
Total frametime: 2066ms, satelite moving: 333ms, space coloring: 1726ms.
Total frametime: 2068ms, satelite moving: 334ms, space coloring: 1727ms.
Total frametime: 2066ms, satelite moving: 333ms, space coloring: 1726ms.
Total frametime: 2066ms, satelite moving: 333ms, space coloring: 1726ms.
Total frametime: 2065ms, satelite moving: 333ms, space coloring: 1725ms.
Total frametime: 2070ms, satelite moving: 332ms, space coloring: 1730ms.
Total frametime: 2071ms, satelite moving: 333ms, space coloring: 1731ms.
```

#### Test machine 3
`Visual studio, project property, C/C++, Optimization, Maximum optimization -O2`

```
Total frametime: 3180ms, satelite moving: 248ms, space coloring: 1511ms.
Total frametime: 1484ms, satelite moving: 114ms, space coloring: 1365ms.
Total frametime: 1511ms, satelite moving: 117ms, space coloring: 1392ms.
Total frametime: 1535ms, satelite moving: 125ms, space coloring: 1395ms.
Total frametime: 1506ms, satelite moving: 115ms, space coloring: 1391ms.
Total frametime: 1517ms, satelite moving: 118ms, space coloring: 1399ms.
Total frametime: 1518ms, satelite moving: 116ms, space coloring: 1402ms.
Total frametime: 1537ms, satelite moving: 117ms, space coloring: 1420ms.
Total frametime: 1533ms, satelite moving: 115ms, space coloring: 1417ms.
Total frametime: 1531ms, satelite moving: 116ms, space coloring: 1414ms.
```

### 3. Original code with also vectorization optimization in the compiler (-ftree-vectorize)
`gcc -o ./build/parallel_vectorized ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec`

#### Test machine 1
The compiler was able to vectorize the copying and conversion operations inside the loop that starts on line 135.
It was also able to apply some optimizations the same optimization to the same part of the code that our answers are compared to, and some parts of the satellite initialization.
However, it was not able to vectorize any loops.

```
parallel_original.c:135:4: note: basic block vectorized
parallel_original.c:345:4: note: basic block vectorized
parallel_original.c:489:4: note: basic block vectorized
```

```
Total frametime: 1131ms, satelite moving: 139ms, space coloring: 472ms.
Total frametime: 553ms, satelite moving: 62ms, space coloring: 480ms.
Total frametime: 551ms, satelite moving: 62ms, space coloring: 484ms.
Total frametime: 533ms, satelite moving: 62ms, space coloring: 466ms.
Total frametime: 544ms, satelite moving: 62ms, space coloring: 477ms.
Total frametime: 537ms, satelite moving: 62ms, space coloring: 470ms.
Total frametime: 541ms, satelite moving: 62ms, space coloring: 473ms.
Total frametime: 535ms, satelite moving: 62ms, space coloring: 467ms.
Total frametime: 539ms, satelite moving: 62ms, space coloring: 472ms.
Total frametime: 536ms, satelite moving: 62ms, space coloring: 468ms.
```

#### Test machine 2
It appears that the older gcc was not able to vectorize as well.

```
parallel_original.c:135:4: note: basic block vectorized                                                                              
parallel_original.c:345:4: note: basic block vectorized
```

```
Total frametime: 4325ms, satelite moving: 681ms, space coloring: 1765ms.
Total frametime: 2114ms, satelite moving: 340ms, space coloring: 1762ms.
Total frametime: 2111ms, satelite moving: 339ms, space coloring: 1764ms.
Total frametime: 2120ms, satelite moving: 340ms, space coloring: 1772ms.
Total frametime: 2118ms, satelite moving: 343ms, space coloring: 1768ms.
Total frametime: 2115ms, satelite moving: 340ms, space coloring: 1767ms.
Total frametime: 2118ms, satelite moving: 342ms, space coloring: 1768ms.
Total frametime: 2118ms, satelite moving: 341ms, space coloring: 1769ms.
Total frametime: 2119ms, satelite moving: 343ms, space coloring: 1768ms.
Total frametime: 2117ms, satelite moving: 342ms, space coloring: 1767ms.
```

#### Test machine 3
`Visual studio, project property, C/C++, command line, /Qvec-report:2`

```
Total frametime: 3107ms, satelite moving: 255ms, space coloring: 1438ms.
Total frametime: 1481ms, satelite moving: 115ms, space coloring: 1361ms.
Total frametime: 1508ms, satelite moving: 123ms, space coloring: 1376ms.
Total frametime: 1487ms, satelite moving: 116ms, space coloring: 1368ms.
Total frametime: 1505ms, satelite moving: 114ms, space coloring: 1391ms.
Total frametime: 1537ms, satelite moving: 121ms, space coloring: 1416ms.
Total frametime: 1556ms, satelite moving: 117ms, space coloring: 1438ms.
Total frametime: 1543ms, satelite moving: 120ms, space coloring: 1422ms.
Total frametime: 1494ms, satelite moving: 119ms, space coloring: 1375ms.
Total frametime: 1493ms, satelite moving: 117ms, space coloring: 1376ms.
```

### 4. Original code with custom compiler flags
#### With fast math
`gcc -o ./build/parallel_relaxed ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math`

Enabling fast math improved vectorization on all tested machines, as it allows the compiler to reorder the floating-point calculations.

Test machine 1

```
parallel_original.c:148:7: note: loop vectorized
parallel_original.c:135:4: note: basic block vectorized
parallel_original.c:358:7: note: loop vectorized
parallel_original.c:345:4: note: basic block vectorized
parallel_original.c:489:4: note: basic block vectorized
```

```
Total frametime: 739ms, satelite moving: 58ms, space coloring: 323ms.
Total frametime: 358ms, satelite moving: 25ms, space coloring: 322ms.
Total frametime: 351ms, satelite moving: 24ms, space coloring: 322ms.
Total frametime: 341ms, satelite moving: 23ms, space coloring: 313ms.
Total frametime: 354ms, satelite moving: 24ms, space coloring: 325ms.
Total frametime: 342ms, satelite moving: 24ms, space coloring: 312ms.
Total frametime: 342ms, satelite moving: 23ms, space coloring: 314ms.
Total frametime: 346ms, satelite moving: 25ms, space coloring: 316ms.
Total frametime: 344ms, satelite moving: 26ms, space coloring: 313ms.
Total frametime: 342ms, satelite moving: 25ms, space coloring: 312ms.
```

#### Test machine 2
```
parallel_original.c:148:7: note: loop vectorized                                                                                     
parallel_original.c:135:4: note: basic block vectorized
parallel_original.c:358:7: note: loop vectorized
parallel_original.c:345:4: note: basic block vectorized
```

```
Total frametime: 2163ms, satelite moving: 180ms, space coloring: 949ms.
Total frametime: 1047ms, satelite moving: 89ms, space coloring: 946ms.
Total frametime: 1043ms, satelite moving: 89ms, space coloring: 947ms.
Total frametime: 1043ms, satelite moving: 89ms, space coloring: 947ms.
Total frametime: 1042ms, satelite moving: 89ms, space coloring: 946ms.
Total frametime: 1043ms, satelite moving: 89ms, space coloring: 947ms.
Total frametime: 1043ms, satelite moving: 88ms, space coloring: 947ms.
Total frametime: 1043ms, satelite moving: 89ms, space coloring: 947ms.
Total frametime: 1044ms, satelite moving: 89ms, space coloring: 948ms.
Total frametime: 1045ms, satelite moving: 89ms, space coloring: 948ms.
```

#### Test machine 3
`Visual studio, project property, C/C++, Optimization, Maximum optimization -O2, Command line /Qvec-report:{1}{2} /fp:fast`

```
Total frametime: 915ms, satelite moving: 242ms, space coloring: 326ms.
Total frametime: 404ms, satelite moving: 114ms, space coloring: 287ms.
Total frametime: 397ms, satelite moving: 108ms, space coloring: 287ms.
Total frametime: 392ms, satelite moving: 107ms, space coloring: 284ms.
Total frametime: 396ms, satelite moving: 108ms, space coloring: 286ms.
Total frametime: 406ms, satelite moving: 107ms, space coloring: 298ms.
Total frametime: 482ms, satelite moving: 110ms, space coloring: 288ms.
Total frametime: 396ms, satelite moving: 108ms, space coloring: 288ms.
Total frametime: 395ms, satelite moving: 107ms, space coloring: 288ms.
Total frametime: 392ms, satelite moving: 106ms, space coloring: 285ms.
```

#### With fast math and AVX2
`gcc -o ./build/parallel_avx2 ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -mavx2`

Test machine 1

```
parallel_original.c:148:7: note: loop vectorized
parallel_original.c:135:4: note: basic block vectorized
parallel_original.c:358:7: note: loop vectorized
parallel_original.c:345:4: note: basic block vectorized
parallel_original.c:489:4: note: basic block vectorized
```

```
Total frametime: 702ms, satelite moving: 41ms, space coloring: 309ms.
Total frametime: 336ms, satelite moving: 19ms, space coloring: 306ms.
Total frametime: 324ms, satelite moving: 18ms, space coloring: 301ms.
Total frametime: 327ms, satelite moving: 19ms, space coloring: 303ms.
Total frametime: 328ms, satelite moving: 18ms, space coloring: 305ms.
Total frametime: 333ms, satelite moving: 18ms, space coloring: 310ms.
Total frametime: 328ms, satelite moving: 18ms, space coloring: 305ms.
Total frametime: 337ms, satelite moving: 18ms, space coloring: 313ms.
Total frametime: 328ms, satelite moving: 18ms, space coloring: 305ms.
Total frametime: 330ms, satelite moving: 19ms, space coloring: 306ms.
```

Test machine 2

```
parallel_original.c:148:7: note: loop vectorized
parallel_original.c:135:4: note: basic block vectorized
parallel_original.c:358:7: note: loop vectorized
parallel_original.c:345:4: note: basic block vectorized
```

As expected, AVX and AVX2 instructions are not supported on the Core i7-930. This can be considered as the "broken" code mentioned in the exercise description.

```
Illegal instruction (core dumped)
```

Test machine 3
`Visual studio, project property, C/C++, Optimization, Maximum optimization -O2,Command line /Qvec-report:{1}{2} /fp:fast /arch:avx2`

```
Total frametime: 914ms, satelite moving: 246ms, space coloring: 317ms.
Total frametime: 404ms, satelite moving: 112ms, space coloring: 287ms.
Total frametime: 395ms, satelite moving: 109ms, space coloring: 284ms.
Total frametime: 399ms, satelite moving: 109ms, space coloring: 289ms.
Total frametime: 392ms, satelite moving: 109ms, space coloring: 282ms.
Total frametime: 394ms, satelite moving: 109ms, space coloring: 283ms.
Total frametime: 394ms, satelite moving: 108ms, space coloring: 284ms.
Total frametime: 409ms, satelite moving: 111ms, space coloring: 286ms.
Total frametime: 396ms, satelite moving: 109ms, space coloring: 287ms.
Total frametime: 403ms, satelite moving: 108ms, space coloring: 289ms.
```

#### For native microarchitecture
These were the fastest settings we found.
It should be noted that compiling for the native microarchitecture should be the fastest by definition, since it enables the use of all the processor features and specifically tunes the binary for the particular microarchitecture.
Linking to OpenMP is enabled, since we have used the same compilation script elsewhere, but it doesn't yet do anything, since there are no OpenMP pragmas in the code.

`gcc -o ./build/parallel_native ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp -march=native -mtune=native`

Test machine 1

```
parallel_original.c:148:7: note: loop vectorized
parallel_original.c:358:7: note: loop vectorized
```

```
Total frametime: 608ms, satelite moving: 40ms, space coloring: 264ms.
Total frametime: 286ms, satelite moving: 18ms, space coloring: 257ms.
Total frametime: 284ms, satelite moving: 19ms, space coloring: 260ms.
Total frametime: 279ms, satelite moving: 18ms, space coloring: 256ms.
Total frametime: 281ms, satelite moving: 17ms, space coloring: 259ms.
Total frametime: 286ms, satelite moving: 17ms, space coloring: 263ms.
Total frametime: 279ms, satelite moving: 17ms, space coloring: 257ms.
Total frametime: 279ms, satelite moving: 16ms, space coloring: 258ms.
Total frametime: 280ms, satelite moving: 17ms, space coloring: 258ms.
Total frametime: 280ms, satelite moving: 17ms, space coloring: 258ms.
```

Test machine 2

```
parallel_original.c:148:7: note: loop vectorized
parallel_original.c:135:4: note: basic block vectorized
parallel_original.c:358:7: note: loop vectorized
parallel_original.c:345:4: note: basic block vectorized
```

```
Total frametime: 2126ms, satelite moving: 179ms, space coloring: 960ms.
Total frametime: 1048ms, satelite moving: 90ms, space coloring: 946ms.
Total frametime: 1041ms, satelite moving: 90ms, space coloring: 944ms.
Total frametime: 1040ms, satelite moving: 89ms, space coloring: 944ms.
Total frametime: 1040ms, satelite moving: 89ms, space coloring: 943ms.
Total frametime: 1040ms, satelite moving: 89ms, space coloring: 944ms.
Total frametime: 1039ms, satelite moving: 89ms, space coloring: 943ms.
Total frametime: 1040ms, satelite moving: 89ms, space coloring: 943ms.
Total frametime: 1040ms, satelite moving: 89ms, space coloring: 944ms.
Total frametime: 1046ms, satelite moving: 89ms, space coloring: 949ms.
```

#### Native with O3
`gcc -o ./build/parallel_native_O3 ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O3 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp -march=native -mtune=native`

Test machine 1

```
parallel_original.c:148:7: note: loop vectorized
parallel_original.c:358:7: note: loop vectorized
```

```
Total frametime: 667ms, satelite moving: 69ms, space coloring: 269ms.
Total frametime: 296ms, satelite moving: 33ms, space coloring: 252ms.
Total frametime: 290ms, satelite moving: 31ms, space coloring: 253ms.
Total frametime: 292ms, satelite moving: 31ms, space coloring: 255ms.
Total frametime: 288ms, satelite moving: 31ms, space coloring: 252ms.
Total frametime: 291ms, satelite moving: 32ms, space coloring: 254ms.
Total frametime: 296ms, satelite moving: 32ms, space coloring: 259ms.
Total frametime: 295ms, satelite moving: 32ms, space coloring: 258ms.
Total frametime: 292ms, satelite moving: 33ms, space coloring: 252ms.
Total frametime: 295ms, satelite moving: 33ms, space coloring: 257ms.
```

Test machine 2

No vectorization change compared to O2, but the performance improved slightly.

```
parallel_original.c:148:7: note: loop vectorized
parallel_original.c:135:4: note: basic block vectorized
parallel_original.c:358:7: note: loop vectorized
parallel_original.c:345:4: note: basic block vectorized
```

```
Total frametime: 2106ms, satelite moving: 180ms, space coloring: 948ms.
Total frametime: 1046ms, satelite moving: 89ms, space coloring: 944ms.
Total frametime: 1039ms, satelite moving: 89ms, space coloring: 943ms.
Total frametime: 1041ms, satelite moving: 89ms, space coloring: 944ms.
Total frametime: 1039ms, satelite moving: 89ms, space coloring: 942ms.
Total frametime: 1041ms, satelite moving: 89ms, space coloring: 944ms.
Total frametime: 1039ms, satelite moving: 89ms, space coloring: 943ms.
Total frametime: 1039ms, satelite moving: 90ms, space coloring: 942ms.
Total frametime: 1040ms, satelite moving: 89ms, space coloring: 943ms.
Total frametime: 1039ms, satelite moving: 89ms, space coloring: 943ms.
```

#### Native with -Ofast
`gcc -o ./build/parallel_native_Ofast ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -Ofast -fopt-info-vec -fopenmp -march=native -mtune=native`

-Ofast includes -O3, -ffast-math, -fno-protect-parens and -fstack-arrays.
In general these are already very aggressive optimizations.
To quote the [Gentoo wiki](https://wiki.gentoo.org/wiki/GCC_optimization), "This option breaks strict standards compliance, and is not recommended for use."

Test machine 1

No more performance gains

```
parallel_original.c:148:7: note: loop vectorized
parallel_original.c:358:7: note: loop vectorized
```

```
Total frametime: 638ms, satelite moving: 70ms, space coloring: 261ms.
Total frametime: 295ms, satelite moving: 32ms, space coloring: 252ms.
Total frametime: 288ms, satelite moving: 30ms, space coloring: 253ms.
Total frametime: 294ms, satelite moving: 32ms, space coloring: 257ms.
Total frametime: 293ms, satelite moving: 31ms, space coloring: 257ms.
Total frametime: 297ms, satelite moving: 31ms, space coloring: 261ms.
Total frametime: 296ms, satelite moving: 31ms, space coloring: 260ms.
Total frametime: 288ms, satelite moving: 30ms, space coloring: 253ms.
Total frametime: 290ms, satelite moving: 31ms, space coloring: 254ms.
Total frametime: 287ms, satelite moving: 30ms, space coloring: 252ms.
```

Test machine 2

No more performance gains.

```
parallel_original.c:148:7: note: loop vectorized
parallel_original.c:135:4: note: basic block vectorized
parallel_original.c:358:7: note: loop vectorized
parallel_original.c:345:4: note: basic block vectorized
```

```
Total frametime: 2189ms, satelite moving: 178ms, space coloring: 949ms.
Total frametime: 1047ms, satelite moving: 90ms, space coloring: 945ms.
Total frametime: 1039ms, satelite moving: 89ms, space coloring: 942ms.
Total frametime: 1040ms, satelite moving: 89ms, space coloring: 943ms.
Total frametime: 1039ms, satelite moving: 89ms, space coloring: 942ms.
Total frametime: 1039ms, satelite moving: 89ms, space coloring: 942ms.
Total frametime: 1039ms, satelite moving: 89ms, space coloring: 943ms.
Total frametime: 1040ms, satelite moving: 88ms, space coloring: 944ms.
Total frametime: 1040ms, satelite moving: 89ms, space coloring: 944ms.
Total frametime: 1040ms, satelite moving: 89ms, space coloring: 944ms.
```

#### Without AVX but with other SIMD instruction set extensions enabled
For older CPUs such as first generation intel Core i7. With these older CPUs the AVX-enabled code would be "broken", since older CPUs don't have support for those instructions. A similar example would be the AVX512 instructions with modern desktop CPUs.

`gcc -o ./build/parallel_no-avx ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp -mmmx -msse -msse2 -msse3 -mssse3 -msse4.1 -msse4.2 -msse4`

Test machine 1

Interestingly the compiler vectorizes more than with `march=native`.
This is presumably caused by such a condition that with these particular CPUs the overhead of the vectorization would result in worse performance than without vectorization, and the compiler would know this.

```
parallel_original.c:148:7: note: loop vectorized
parallel_original.c:135:4: note: basic block vectorized
parallel_original.c:358:7: note: loop vectorized
parallel_original.c:345:4: note: basic block vectorized
parallel_original.c:489:4: note: basic block vectorized
```

```
Total frametime: 776ms, satelite moving: 56ms, space coloring: 333ms.
Total frametime: 351ms, satelite moving: 24ms, space coloring: 316ms.
Total frametime: 350ms, satelite moving: 23ms, space coloring: 322ms.
Total frametime: 343ms, satelite moving: 24ms, space coloring: 314ms.
Total frametime: 348ms, satelite moving: 24ms, space coloring: 319ms.
Total frametime: 352ms, satelite moving: 24ms, space coloring: 320ms.
Total frametime: 346ms, satelite moving: 23ms, space coloring: 318ms.
Total frametime: 340ms, satelite moving: 23ms, space coloring: 312ms.
Total frametime: 350ms, satelite moving: 24ms, space coloring: 321ms.
Total frametime: 348ms, satelite moving: 23ms, space coloring: 319ms.
```

Test machine 2

```
parallel_original.c:148:7: note: loop vectorized
parallel_original.c:135:4: note: basic block vectorized
parallel_original.c:358:7: note: loop vectorized
parallel_original.c:345:4: note: basic block vectorized
```

```
Total frametime: 2153ms, satelite moving: 181ms, space coloring: 963ms.
Total frametime: 1061ms, satelite moving: 90ms, space coloring: 958ms.
Total frametime: 1056ms, satelite moving: 91ms, space coloring: 958ms.
Total frametime: 1058ms, satelite moving: 90ms, space coloring: 960ms.
Total frametime: 1057ms, satelite moving: 91ms, space coloring: 959ms.
Total frametime: 1055ms, satelite moving: 90ms, space coloring: 958ms.
Total frametime: 1055ms, satelite moving: 90ms, space coloring: 957ms.
Total frametime: 1054ms, satelite moving: 90ms, space coloring: 956ms.
Total frametime: 1055ms, satelite moving: 90ms, space coloring: 957ms.
Total frametime: 1055ms, satelite moving: 90ms, space coloring: 958ms.
```

On machine 2 there was a consistent difference of about 10 ms of total frame time with several execution runs.
Therefore it appears that compiling and tuning for the native microarchitecture has a slight benefit over enabling individual instruction set extensions.
Or it could be that we simply forgot some useful instruction set extension from the compiler flags.

### Conclusions

We were surprised by the large difference caused by enabling fast math.
It was also fascinating to see that even CPUs have improved quite a bit over the last decade despite all the talk about the slowdown of Moore's law.
(Please note that test machine 1 and 3 are laptops and test machine 2 is an overclocked desktop.)
We were not surprised that the code compiled with Visual Studio was significantly slower, even though the test machine 3 has [the most powerful CPU](https://cpu.userbenchmark.com/Compare/Intel-Core-i9-8950HK-vs-Intel-Core-i7-8550U/m486215vsm320742).

## 6.2 Generic algorithm optimization

#### Attempted optimizations
- Use of arrays instead of the given structs. Resulted in lower performance.
- Physics loop reordering. Resulted in lower performance, probably due to worse vectorization.
- Moving tmpPosition and tmpVelocity initialization inside the loop and making it for one satellite only. Resulted in lower performance.
- Calculating `DELTATIME / PHYSICSUPDATESPERFRAME` at compile or start time instead of recalculating it at each operation. Resulted in failure of error test, at least with -ffast-math.
- Adding `#pragma omp simd` to the loops had no effect.
- Attempted to use a grid-based satellite search system to remove the need for iterating over each satellite in the graphics satellite loop. This did not work, since the distance to each satellite has to be calculated anyway to determine the variable "weights". However, it would have sped up the total frame time by about 20 %, at least according to quick performance tests. The attempt is in the file "parallel_generic_grid.c"

#### Optimizations not yet attempted
- Using Runge-Kutta instead of Euler integration would remove the need for the physics frame loop. This would likely result in approximately the same results, but due to floating-point differences it would most likely not pass the integrated tests.
- Using explicit vectorization intrinsics

#### Performance 

With improved graphics loops (parallel_generic.c).
OpenMP SIMD pragmas are also enabled, but it appears that they don't have any effect, as gcc is already good enough in utilising SIMD.
Although it's fascinating to note that the compiler vectorizes more for the older CPU, even though it has a smaller SIMD instruction set.
The buggy pixel warning is raised, but the resulting image looks the same as before the modifications.
The warning is therefore most likely due to slight floating-point differences.

`gcc -o ./build/parallel_native ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp -march=native -mtune=native`

Test machine 1

```
parallel_generic.c:149:7: note: loop vectorized
parallel_generic.c:364:7: note: loop vectorized
```

```
Buggy pixel at (x=0, y=0). Press enter to continue.

Total frametime: 3989ms, satelite moving: 39ms, space coloring: 230ms.
Total frametime: 272ms, satelite moving: 25ms, space coloring: 211ms.
Total frametime: 230ms, satelite moving: 16ms, space coloring: 208ms.
Total frametime: 233ms, satelite moving: 18ms, space coloring: 210ms.
Total frametime: 237ms, satelite moving: 17ms, space coloring: 215ms.
Total frametime: 234ms, satelite moving: 18ms, space coloring: 211ms.
Total frametime: 234ms, satelite moving: 18ms, space coloring: 211ms.
Total frametime: 235ms, satelite moving: 17ms, space coloring: 213ms.
Total frametime: 239ms, satelite moving: 17ms, space coloring: 217ms.
Total frametime: 235ms, satelite moving: 18ms, space coloring: 212ms.
```

Test machine 2

```
parallel_generic.c:149:7: note: loop vectorized
parallel_generic.c:138:25: note: basic block vectorized
parallel_generic.c:364:7: note: loop vectorized
parallel_generic.c:351:4: note: basic block vectorized
```

```
Buggy pixel at (x=0, y=0). Press enter to continue.

Total frametime: 3142ms, satelite moving: 179ms, space coloring: 706ms.
Total frametime: 818ms, satelite moving: 91ms, space coloring: 712ms.
Total frametime: 820ms, satelite moving: 90ms, space coloring: 723ms.
Total frametime: 812ms, satelite moving: 90ms, space coloring: 714ms.
Total frametime: 811ms, satelite moving: 92ms, space coloring: 712ms.
Total frametime: 808ms, satelite moving: 90ms, space coloring: 710ms.
Total frametime: 809ms, satelite moving: 90ms, space coloring: 712ms.
Total frametime: 812ms, satelite moving: 90ms, space coloring: 714ms.
Total frametime: 810ms, satelite moving: 91ms, space coloring: 712ms.
Total frametime: 810ms, satelite moving: 91ms, space coloring: 711ms.
```

## 6.3 Code analysis for multi-thread parallelization

#### Parallelization possibilities for each loop
- Loading of satellite positions and velocities (line 135 ->) can be parallelized.
- Physics satellite loop (line 148 ->) can be parallelized, since the satellites don't interact with each other.
- Physics iteration loop cannot be parallelized, since each step is dependent on the previous results.
- Copying of physics results (line 182 ->) can be parallelized.
- Graphics pixel loop (line 197->) can be parallelized, since the pixels don't depend on each other, but only on the physics results.
- The first graphics satellite loop (line 212 ->) could be parallelized, but this would require synchronizing access to some variables such as `weights`, `hitsSatellite`, `shortestDistance` and `renderColor`.
- The second graphics satellite loop (line 235) could be parallelized using a reduction pattern.

However, parallelization of the inner graphics loops is unnecessary, since the outer loop can already be parallelized, providing better performance gains.

Parallelization of the physics engine could be improved by changing the order of the iteration and satellite loops.
This way it would be possible to calculate the complete movement of each satellite in parallel.

It is likely that the parallelization will also affect the vectorization.
Some compiler settings resulted in gcc claiming to be able to vectorize the inner physics loop, which is rather surprising due to the length of the loop element, so changing the code is likely to break that.
However, changing the code in general is likely to result in a better use of data structures (struct -> array?) and temporary variables, and might therefore aid in the vectorization.
It might also be possible to use explicit vectorization intrinsics, which would further aid in the vectorization.

## 6.4 OpenMP parallelization

### Performance
`gcc -o ./build/parallel_native ${SOURCE_FILE} -std=c99 -lglut -lGL -lm -Wall -O2 -ftree-vectorize -fopt-info-vec -ffast-math -fopenmp -march=native -mtune=native`

#### Test machine 1
```
parallel_openmp.c:352:7: note: loop vectorized
```

```
Total frametime: 413ms, satelite moving: 38ms, space coloring: 85ms.
Total frametime: 102ms, satelite moving: 17ms, space coloring: 74ms.
Total frametime: 96ms, satelite moving: 17ms, space coloring: 73ms.
Total frametime: 95ms, satelite moving: 16ms, space coloring: 73ms.
Total frametime: 96ms, satelite moving: 16ms, space coloring: 74ms.
Total frametime: 95ms, satelite moving: 17ms, space coloring: 72ms.
Total frametime: 97ms, satelite moving: 19ms, space coloring: 72ms.
Total frametime: 104ms, satelite moving: 20ms, space coloring: 78ms.
Total frametime: 103ms, satelite moving: 19ms, space coloring: 78ms.
Total frametime: 100ms, satelite moving: 17ms, space coloring: 77ms.
```

This is about 2.8 times as fast as without parallelization.
This is on the order of magnitude of the number of cores (4), but not close.
There is always some overhead with multi-threading, and therefore the speedup cannot be exactly the number of cores.
However, since this machine is a laptop, it's probably hitting its power and temperature throttling limits.

#### Test machine 2
```
parallel_openmp.c:178:31: note: basic block vectorized
parallel_openmp.c:352:7: note: loop vectorized
parallel_openmp.c:339:4: note: basic block vectorized
```

```
Total frametime: 1358ms, satelite moving: 141ms, space coloring: 230ms.
Total frametime: 286ms, satelite moving: 46ms, space coloring: 228ms.
Total frametime: 285ms, satelite moving: 48ms, space coloring: 229ms.
Total frametime: 281ms, satelite moving: 46ms, space coloring: 227ms.
Total frametime: 287ms, satelite moving: 47ms, space coloring: 232ms.
Total frametime: 284ms, satelite moving: 47ms, space coloring: 229ms.
Total frametime: 294ms, satelite moving: 53ms, space coloring: 232ms.
Total frametime: 284ms, satelite moving: 47ms, space coloring: 227ms.
Total frametime: 279ms, satelite moving: 46ms, space coloring: 226ms.
Total frametime: 280ms, satelite moving: 45ms, space coloring: 227ms.
```

This is about 3.5 times as fast as without parallelization.
This is almost as much as the number of cores (4).

### Code changes

#### Implemented changes
- Changed the order of the physics loops
- Parallelised the outer physics and graphics loops
- Added OpenMP SIMD pragmas

Parallelization of the inner loops would not be beneficial, as they have several dependencies.
Therefore parallelizing them without other changes would most likely result in race conditions and therefore undefined behaviour.
The inner graphics loop bodies are also quite small, and a slowdown would be possible by attempting to parallelise them.

<!--
Test machine 3:
OpenMP header file is need to be added (#include <omp.h>) and it can be implemented by adding #pragma omp parallel num_threads(SATELITE_COUNT) before loop. and #pragma optimize( " simd ", { on } ) is added before parallelGraphicsEngine() and parallelPhysicsEngine methods.
In parallelPhysicsEngine, two loop which are used for float storage are parallelized.
when parallelization was added to the most inner loop of the methods, the visual studio program crashed.
-->
