#ifdef _WIN32
#include <windows.h>
#endif
#include <CL/cl.h>
#include <stdio.h>

cl_program build_program(const cl_context ctx, const cl_device_id dev, const char* filename);
void check_status(const cl_int status);
void enable_windows_console();
char* clGetDeviceInfoStr(const cl_device_id device, const cl_device_info param_name);
char* clGetPlatformInfoStr(const cl_platform_id platform, const cl_platform_info param_name);
