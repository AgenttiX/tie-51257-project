__kernel
void graphics(
        // Flattened so that (r, g, b) of each pixel are sequential
        __global float* restrict pixels,
        // Flattened so that (r, g, b) of each satellite are sequential
        __constant float* restrict satellite_colors,
        __constant float* restrict satellite_pos_x,
        __constant float* restrict satellite_pos_y,
        const int window_width,
        const int satellite_count,
        const float satellite_radius) {
    // Pixel index
    const int i = get_global_id(0);

    const float x = i % window_width;
    const float y = i / window_width;

    float r = 0.f;
    float g = 0.f;
    float b = 0.f;

    float shortestDistance = INFINITY;

    float weights = 0.f;
    bool hitsSatellite = false;

    // Find the closest satellite
    for(int j = 0; j < satellite_count; ++j) {
        // const int pos_ind = 2*j;
        const float diff_x = x - satellite_pos_x[j];
        const float diff_y = y - satellite_pos_y[j];
        const float dist = sqrt(diff_x * diff_x + diff_y * diff_y);

        if(dist < satellite_radius) {
            r = 1.0f;
            g = 1.0f;
            b = 1.0f;
            hitsSatellite = true;
            break;
        } else {
            const float weight = 1.0f / (dist*dist*dist*dist);
            weights += weight;
            if(dist < shortestDistance) {
                shortestDistance = dist;
                const int sat_color_ind = 3*j;
                r = satellite_colors[sat_color_ind];
                g = satellite_colors[sat_color_ind+1];
                b = satellite_colors[sat_color_ind+2];
            }
        }
    }

    if(!hitsSatellite) {
        for(int j = 0; j < satellite_count; ++j) {
            // const int pos_ind = 2*j;
            const float diff_x = x - satellite_pos_x[j];
            const float diff_y = y - satellite_pos_y[j];
            const float dist2 = (diff_x*diff_x + diff_y*diff_y);
            const float weight = 1.0f/(dist2*dist2);
            const int sat_color_ind = 3*j;
            r += (satellite_colors[sat_color_ind] * weight / weights) * 3.0f;
            g += (satellite_colors[sat_color_ind+1] * weight / weights) * 3.0f;
            b += (satellite_colors[sat_color_ind+2] * weight / weights) * 3.0f;
        }
    }
    const int pixel_ind = 3*i;
    pixels[pixel_ind] = r;
    pixels[pixel_ind+1] = g;
    pixels[pixel_ind+2] = b;
}
