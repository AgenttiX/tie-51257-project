@echo off
where cl
IF %ERRORLEVEL% NEQ 0 (
    echo Visual Studio build tools were not found. Loading them now.
    call "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvars64.bat"^
     || call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat"^
     || call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat"^
     || call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\VC\Auxiliary\Build\vcvars64.bat"^
     || echo Visual studio build tools were not found!
)
powershell -File .\build_windows_msvc\build.ps1
