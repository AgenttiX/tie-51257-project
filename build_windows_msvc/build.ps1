﻿# Get the source filename
$source_file = (Get-Item Env:SOURCE_FILE).Value
if( [string]::IsNullOrEmpty($source_File)) {
    Write-Host "Please set the SOURCE_FILE environment variable"
    exit
}
Write-Host "Building using $source_file"

$cwd = Get-Location

# Download FreeGLUT
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12;
if(! (Test-Path $cwd\temp\freeglut-MSVC)) {
    mkdir $cwd\temp
    Invoke-WebRequest https://www.transmissionzero.co.uk/files/software/development/GLUT/freeglut-MSVC.zip -OutFile $cwd\temp\freeglut-MSVC.zip
    Expand-Archive -Path $cwd\temp\freeglut-MSVC.zip -DestinationPath $cwd\temp\freeglut-MSVC;
}
# Make the build directory if necessary
if(! (Test-Path $cwd\build)) {
    mkdir $cwd\build
}
# Copy FreeGLUT DLL to the build folder so that the built binaries will be able to find it
if(! (Test-Path $cwd\build\freeglut.dll)) {
    cp $cwd\temp\freeglut-MSVC\freeglut\bin\x64\freeglut.dll $cwd\build\
}
# Remove old build files just in case
rm $cwd\build\*.obj
rm $cwd\build\*.exe
# Change to the build directory to ensure that all output files are stored there
cd $cwd\build
# Note that using /fp:fast may result in slight floating-point inaccuracies for the tests
cl "$cwd\$source_file" /O2 /arch:AVX2 /fp:fast /I"$cwd\temp\freeglut-MSVC\freeglut\include" /I"C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v10.1\include" /I"C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v10.2\include" OpenCL.lib /link /LIBPATH:$cwd"\temp\freeglut-MSVC\freeglut\lib\x64" /LIBPATH:"C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v10.1\lib\x64" /LIBPATH:"C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v10.2\lib\x64"
